#include "tokenauth.h"

#include <time.h>
#include <sodium.h>
#include <DBLib/BasicFile.h>
#include <DBLib/TransactionalFile.h>
#include <DBLib/AllocatingFile.h>
#include <stdexcept>

extern volatile char ServerShouldDie;

char* Base64Chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_";
#define ToBase64(n) Base64Chars[n]
char FromBase64 (char ch) // If ch is an unrecognized character, returns 64.
{
	if (ch >= '0' && ch <= '9') return ch - '0'; // 0 to 9
	if (ch >= 'a' && ch <= 'z') return ch - 'a' + 10; // 10 to 35
	if (ch >= 'A' && ch <= 'Z') return ch - 'A' + 36; // 36 to 61
	if (ch == '-') return 62; // 62
	if (ch == '_') return 63; // 63
	return 64; // Invalid
};

char* GetCookie (Client* client, char* cookie_name)
{
	NameValuePair* cookie = client->cookies.pairs;
	for (size_t i = 0; i < client->cookies.count; i++, cookie++)
		if (!strcmp(cookie->name,cookie_name))
			return cookie->value;
	return NULL;
};

char ValidUserChar (char cc)
{
	if (cc >= 'a' && cc <= 'z') return 1;
	if (cc >= 'A' && cc <= 'Z') return 1;
	if (cc >= '0' && cc <= '9') return 1;
	if (cc == '_') return 1;
	return 0;
};

char* ValidateUsername (char* username)
{
	size_t user_len = strlen(username);
	if (user_len < MIN_USERNAME) return "Username too short.";
	if (user_len > MAX_USERNAME) return "Username too long.";

	while (user_len--)
		if (!ValidUserChar(*username++))
			return "Invalid username character. Must be a-z, A-Z, 0-9, or _.";

	return NULL;
};

char* ValidatePassword (char* password, size_t passlen)
{
	if (passlen < MIN_PASSWORD) return "Password too short.";

	char has_alpha = 0, has_digit = 0, has_symbol = 0;
	while (passlen--)
	{
		char cc = *password++;
		if (cc >= 'a' && cc <= 'z') has_alpha = 1;
		else if (cc >= 'A' && cc <= 'Z') has_alpha = 1;
		else if (cc >= '0' && cc <= '9') has_digit = 1;
		else has_symbol = 1;
	};
	char diverse = has_alpha && has_digit && has_symbol;
	if (!diverse) return "Password must contain a letter, a digit, and a symbol.";

	return NULL;
};

UserPassPair InterpretUserPass (Client* client)
{
	PostData postdata = GetPostData(client,1024);
	UserPassPair out;

	size_t inspect_pos = 0;
	out.username = postdata.buf;
	while (1)
	{
		if (inspect_pos >= postdata.length)
		{
			HTTP_ResponseHeaders(client,"422 Unprocessable Entity","text/plain");
			HTTP_SendStringContent(client,"POST data is malformed. Expected: \"username;password\"");
			return {0};
		};

		if (postdata.buf[inspect_pos] == USERNAME_PASSWORD_SEPARATOR) break;
		inspect_pos++;
	};
	postdata.buf[inspect_pos++] = 0;

	out.password = postdata.buf + inspect_pos;
	out.passlen = postdata.length - inspect_pos;

	return out;
};



/// Concurrent reads, synchronized writes, no reads during writes.
SDL_mutex* WritePrevention;
SDL_mutex* WriteLock;
SDL_atomic_t Accessors = {0};
void PreventWrite () /// Call before reading.
{
	SDL_LockMutex(WritePrevention);
	if (!SDL_AtomicIncRef(&Accessors)) SDL_LockMutex(WriteLock);
	SDL_UnlockMutex(WritePrevention);
};
void AllowWrite () /// Call after reading.
{
	if (SDL_AtomicDecRef(&Accessors)) SDL_UnlockMutex(WriteLock);
};
void LockWrite () /// Call before writing.
{
	SDL_LockMutex(WritePrevention);
	SDL_LockMutex(WriteLock);
	SDL_UnlockMutex(WritePrevention);
};
void UnlockWrite () /// Call after writing.
{
	SDL_UnlockMutex(WriteLock);
};



/// IMPORTANT: changing these four structures will screw up any existing account+token+names file
// should consider a more flexible design to increase compatibility with older versions

typedef struct
{
	size_t n_accounts;
		// or in other words, the ID which will be assigned to the next account created.

	Sint64 by_name_root;
		// root of binary search tree of accounts sorted by username
}
AccountsHeader;

typedef struct
{
	size_t name_addr,name_len;
		// Address of user name within NamesFile

	Sint64 by_name_left,by_name_right;
		// BST left & right nodes for searching by username

	char pass_salt [crypto_pwhash_SALTBYTES];
		// Does crypto_pwhash_SALTBYTES change for different libsodium versions?
		// that would cause major problems

	char pass_hash [SMHAUTH_PWHASH_LEN];
		// Problems if SMHAUTH_PWHASH_LEN definition is modified.

	Sint64 first_user_token,last_user_token;
	size_t n_user_tokens;
		// linked list & length to keep track of the tokens for this user
		// so, when too many are generated, the oldest one is deactivated
		// avoids denial-of-service attack from spamming log-in without log-out
}
Account;

typedef struct
{
	/// The "active" list keeps track of occupied positions in order, with the first one having soonest expiry.
	/// 'Refreshing' a token removes it and re-adds it to the end of the active list.
	Sint64 first_active_id; // -1 for null
	Sint64 last_active_id;

	/// ID of first unoccupied position to be "allocated" next, or -1.
	Sint64 first_available_id; // may be -1 for null if the current array is saturated.

	/// Number of positions in the array. Expanding the array requires appending all new spaces to the 'available' list.
	Sint64 current_capacity; // guaranteed >= 1
}
TokensHeader;

typedef struct
{
	char token [TOKEN_DATA_LENGTH];

	time_t last_used;

	Sint64 account_id; /// Must set this to -1 for inactive tokens.

	/// Next unused position, or -1.
	union
	{
		// these are unioned because an 'available' token will never be in the active list
		Sint64 next_available_id; // -1 for null
		Sint64 next_active_id; // -1 for null
	};
	Sint64 prev_active_id; // -1 for null

	Sint64 prev_user_token,next_user_token;
}
Token;

BasicFile* RawFile;
TransactionalFile* Transactions;
ArraysFileHost* TokensAccountsHost;
LinearFile* AccountsFile;
LinearFile* TokensFile;
LinearFile* NamesFile;
AllocatingFile* NamesAllocator;

void TokenGlobalActiveRemove (Token* token) /// Very low level. Does not activate locks.
{
	// Remove from global active tokens list.
	if (token->prev_active_id >= 0)
	{
		// Write next-pointer of previous token. Set to next token.
		TokensFile->Write(
			&token->next_active_id,
			sizeof(TokensHeader) + sizeof(Token) * token->prev_active_id + offsetof(Token,next_active_id),
			8
		);
	}
	else
	{
		// Write first-pointer of tokens header. Set to next token.
		TokensFile->Write(
			&token->next_active_id,
			sizeof(TokensHeader) + offsetof(TokensHeader,first_active_id),
			8
		);
	};
	if (token->next_active_id >= 0)
	{
		// Write prev-pointer of next token. Set to previous token.
		TokensFile->Write(
			&token->prev_active_id,
			sizeof(TokensHeader) + sizeof(Token) * token->next_active_id + offsetof(Token,prev_active_id),
			8
		);
	}
	else
	{
		// Write last-pointer of tokens header. Set to previous token.
		TokensFile->Write(
			&token->prev_active_id,
			sizeof(TokensHeader) + offsetof(TokensHeader,last_active_id),
			8
		);
	};
};
void TokenGlobalActiveAdd (Sint64 token_id) /// Very low level. Does not activate locks.
{
	TokensHeader header;
	TokensFile->Read(&header,0,sizeof(TokensHeader));
	Sint64 neg1 = -1;
	if (header.first_active_id >= 0)
	{
		// Write prev-pointer of new item. Set to existing last.
		TokensFile->Write(
			&header.last_active_id,
			sizeof(TokensHeader) + sizeof(Token) * token_id + offsetof(Token,prev_active_id),
			8
		);
		// Write next-pointer of existing last. Set to new item.
		TokensFile->Write(
			&token_id,
			sizeof(TokensHeader) + sizeof(Token) * header.last_active_id + offsetof(Token,next_active_id),
			8
		);
		// Write last-pointer of header. Set to new item.
		TokensFile->Write(
			&token_id,
			offsetof(TokensHeader,last_active_id),
			8
		);
	}
	else
	{
		// Write prev-pointer of new item. Set to -1.
		TokensFile->Write(
			&neg1,
			sizeof(TokensHeader) + sizeof(Token) * token_id + offsetof(Token,prev_active_id),
			8
		);
		// Write first-pointer of header. Set to new item.
		TokensFile->Write(
			&token_id,
			offsetof(TokensHeader,first_active_id),
			8
		);
		// Write last-pointer of header. Set to new item.
		TokensFile->Write(
			&token_id,
			offsetof(TokensHeader,last_active_id),
			8
		);
	};
	// Write next-pointer of new item. Set to -1.
	TokensFile->Write(
		&neg1,
		sizeof(TokensHeader) + sizeof(Token) * token_id + offsetof(Token,next_active_id),
		8
	);
};
void AccountTokensRemove (Token* token, Sint64 account_id) /// Very low level. Does not activate locks.
{
	//printf("prev user token %lld\n",token->prev_user_token);
	//printf("next user token %lld\n",token->next_user_token);
	// Remove from the account's tokens list.
	if (token->prev_user_token >= 0)
	{
		// Write next-pointer of previous token. Set to next token.
		TokensFile->Write(
			&token->next_user_token,
			sizeof(TokensHeader) + sizeof(Token) * token->prev_user_token + offsetof(Token,next_user_token),
			8
		);
	}
	else
	{
		// Write first-pointer of account. Set to next token.
		AccountsFile->Write(
			&token->next_user_token,
			sizeof(AccountsHeader) + sizeof(Account) * account_id + offsetof(Account,first_user_token),
			8
		);
	};
	if (token->next_user_token >= 0)
	{
		// Write prev-pointer of next token. Set to previous token.
		TokensFile->Write(
			&token->prev_user_token,
			sizeof(TokensHeader) + sizeof(Token) * token->next_user_token + offsetof(Token,prev_user_token),
			8
		);
	}
	else
	{
		// Write last-pointer of account. Set to previous token.
		AccountsFile->Write(
			&token->prev_user_token,
			sizeof(AccountsHeader) + sizeof(Account) * account_id + offsetof(Account,last_user_token),
			8
		);
	};
};
void AccountTokensAdd (Sint64 token_id, Sint64 account_id) /// Very low level. Does not activate locks.
{
	Account account;
	AccountsFile->Read(&account,sizeof(AccountsHeader) + sizeof(Account) * account_id,sizeof(Account));
	Sint64 neg1 = -1;
	if (account.first_user_token >= 0)
	{
		// Write prev-pointer of new item. Set to existing last.
		TokensFile->Write(
			&account.last_user_token,
			sizeof(TokensHeader) + sizeof(Token) * token_id + offsetof(Token,prev_user_token),
			8
		);
		// Write next-pointer of existing last. Set to new item.
		TokensFile->Write(
			&token_id,
			sizeof(TokensHeader) + sizeof(Token) * account.last_user_token + offsetof(Token,next_user_token),
			8
		);
		// Write last-pointer of account. Set to new item.
		TokensFile->Write(
			&token_id,
			sizeof(AccountsHeader) + sizeof(Account) * account_id + offsetof(Account,last_user_token),
			8
		);
	}
	else
	{
		// Write prev-pointer of new item. Set to -1.
		TokensFile->Write(
			&neg1,
			sizeof(TokensHeader) + sizeof(Token) * token_id + offsetof(Token,prev_user_token),
			8
		);
		// Write first-pointer of account. Set to new item.
		TokensFile->Write(
			&token_id,
			sizeof(AccountsHeader) + sizeof(Account) * account_id + offsetof(Account,first_user_token),
			8
		);
		// Write last-pointer of account. Set to new item.
		TokensFile->Write(
			&token_id,
			sizeof(AccountsHeader) + sizeof(Account) * account_id + offsetof(Account,last_user_token),
			8
		);
	};
	// Write next-pointer of new item. Set to -1.
	TokensFile->Write(
		&neg1,
		sizeof(TokensHeader) + sizeof(Token) * token_id + offsetof(Token,next_user_token),
		8
	);
};



Sint64 LookupUsername (char* username, size_t* downptr_addr) /// Low level. Does not activate locks.
{
	int len = strlen(username);
	AccountsHeader accounts;
	AccountsFile->Read(&accounts,0,sizeof(AccountsHeader));
	if (downptr_addr) *downptr_addr = offsetof(AccountsHeader,by_name_root);
	Sint64 account_id = accounts.by_name_root;
	while (account_id >= 0)
	{
		printf("traversing account id %lld\n",account_id);
		Account account;
		AccountsFile->Read(&account,sizeof(AccountsHeader) + sizeof(Account) * account_id,sizeof(Account));
		int cmp_len = len < account.name_len ? len : account.name_len;
		signed char cmp_result = NamesFile->ContentCompare(username,account.name_addr,cmp_len);
		if (!cmp_result)
		{
			if (account.name_len == len) return account_id; // Exact match.
			cmp_result = len > account.name_len ? 1 : -1; // Longer name is sorted later, shorter name is sorted earlier.
		};
		if (downptr_addr) *downptr_addr = sizeof(AccountsHeader) + sizeof(Account) * account_id + (cmp_result > 0 ? offsetof(Account,by_name_right) : offsetof(Account,by_name_left));
		printf("left %lld right %lld\n",account.by_name_left,account.by_name_right);
		account_id = cmp_result > 0 ? account.by_name_right : account.by_name_left;
		printf("set account id %lld\n",account_id);
	};
	return -1;
};

void DeactivateToken (Sint64 token_id)
{
	/*
		Remove from active list
		Remove from account's token list
		Decrease account tokens count
		Explicitly write -1 to the account_id
		Add to available list for reuse
	*/

    TokensHeader header;
    TokensFile->Read(&header,0,sizeof(TokensHeader));

	Token token;
	TokensFile->Read(&token,sizeof(TokensHeader) + sizeof(Token) * token_id,sizeof(Token));
	Sint64 account_id = token.account_id;
	token.account_id = -1;
	token.next_available_id = header.first_available_id;
	TokensFile->Write(&token,sizeof(TokensHeader) + sizeof(Token) * token_id,sizeof(Token));

	header.first_available_id = token_id;
	TokensFile->Write(&header,0,sizeof(TokensHeader));

	printf("global actives remove\n");
	TokenGlobalActiveRemove(&token);
	printf("account tokens remove\n");
	AccountTokensRemove(&token,account_id);
	printf("other updating\n");
	Account account;
	AccountsFile->Read(&account,sizeof(AccountsHeader) + sizeof(Account) * account_id,sizeof(Account));
	account.n_user_tokens--;
	AccountsFile->Write(&account,sizeof(AccountsHeader) + sizeof(Account) * account_id,sizeof(Account));
};

Sint64 AccountNewToken (Sint64 account_id)
{
	/*
		Take from the available list
			If empty, expand the array first
		Add to active list
		Add to account's token list
		Increase account tokens count
		If account tokens count is too high:
			Deactivate first token in account's token list
	*/

	TokensHeader header;
	TokensFile->Read(&header,0,sizeof(TokensHeader));
	Sint64 token_id = header.first_available_id;
	if (token_id >= 0)
	{
		Token token;
		TokensFile->Read(&token,sizeof(TokensHeader) + sizeof(Token) * token_id,sizeof(Token));
		header.first_available_id = token.next_available_id;
	}
	else
	{
		token_id = header.current_capacity;
		header.current_capacity++;
	};
	TokensFile->Write(&header,0,sizeof(TokensHeader));

	TokenGlobalActiveAdd(token_id);
	AccountTokensAdd(token_id,account_id);
	Account account;
	AccountsFile->Read(&account,sizeof(AccountsHeader) + sizeof(Account) * account_id,sizeof(Account));
	account.n_user_tokens++;
	AccountsFile->Write(&account,sizeof(AccountsHeader) + sizeof(Account) * account_id,sizeof(Account));

	if (account.n_user_tokens > MAX_ACCOUNT_TOKENS)
		DeactivateToken(account.first_user_token);

	return token_id;
};



Sint64 AccountPasswordVerify (UserPassPair userpass) /// Low level. Activates locks.
{
	///sodium_mlock(userpass.password,userpass.passlen);

	printf("prevent write\n");
	PreventWrite();

	printf("lookup\n");
	Sint64 account_id = LookupUsername(userpass.username,NULL);
	if (account_id < 0)
	{
		AllowWrite(); // Don't leave this locked, that would be quite a nasty bug.
		return -1;
	};
	Account account;
	AccountsFile->Read(&account,sizeof(AccountsHeader) + sizeof(Account) * account_id,sizeof(Account));

	AllowWrite();

	printf("hash\n");
	char hash [SMHAUTH_PWHASH_LEN];
	int hash_result =
		crypto_pwhash(
			hash,SMHAUTH_PWHASH_LEN,
			userpass.password,userpass.passlen,
			account.pass_salt,
			crypto_pwhash_OPSLIMIT_MODERATE,crypto_pwhash_MEMLIMIT_MODERATE,
			crypto_pwhash_ALG_DEFAULT /// if crypto_pwhash_ALG_DEFAULT changes between versions this could cause big problems?
		);

	///sodium_munlock(userpass.password,userpass.passlen);

	if (hash_result) // nonzero result means failed
	{
		printf("ERROR: hash computation failure (during login)\n");
		return -1;
	};

	printf("compare\n");
	// compute whether hash is same
	for (int i = 0; i < SMHAUTH_PWHASH_LEN; i++)
		if (hash[i] != account.pass_hash[i])
			return -1; // mismatch

	return account_id; // all matching, password is correct
};

char* CookieConstText =
	"; Max-Age="TOKEN_MAX_AGE_STRING // Token lifetime (otherwise would go away when browser closes).
	#ifdef SMHAUTH_HTTPS_ONLY
	"; Secure" // Cookie is HTTPS only.
	#endif
	"; HttpOnly" // Cookie should not be available to JavaScript.
	"; SameSite=Strict" // Do not send this cookie to other services.
	"\r\n";
void SendSetCookie (Client* client, Sint64 token_id, char* token_content)
{
	char id_chars [TOKEN_IDSTR_LENGTH];
	for (int i = TOKEN_IDSTR_LENGTH; i; )
	{
		Uint8 value = token_id & 0x3F;
		id_chars[--i] = ToBase64(value);
		token_id >>= 6;
	};
	SendString(client,"Set-Cookie: "SMHAUTH_COOKIE_NAME"=");
	SDLNet_TCP_Send(client->client_socket,id_chars,TOKEN_IDSTR_LENGTH);
	SDLNet_TCP_Send(client->client_socket,token_content,TOKEN_DATA_LENGTH);
	SendString(client,CookieConstText);
};

void LogIn (Client* client) /// High level. Activates locks.
{
	printf("interpret\n");
	UserPassPair userpass = InterpretUserPass(client);
	if (!userpass.username) return; // InterpretUserPass already sent an HTTP response message.

	printf("verify\n");
	Sint64 account_id = AccountPasswordVerify(userpass);

	if (account_id >= 0)
	{
		printf("lock write\n");
		LockWrite(); // If usernames could be destroyed, danger of account destruction before write lock is acquired.

		/*
			Get new token
			Randomize token content
			Send cookie
		*/
		printf("account new token\n");
		Sint64 token_id = AccountNewToken(account_id);
		size_t token_addr = sizeof(TokensHeader) + sizeof(Token) * token_id;

		Token token;
		TokensFile->Read(&token,token_addr,sizeof(Token));

		printf("randomize\n");
		randombytes_buf(token.token,TOKEN_DATA_LENGTH);
		for (int i = 0; i < TOKEN_DATA_LENGTH; i++)
			token.token[i] = ToBase64(token.token[i] & 0x3F);

		token.account_id = account_id;
		token.last_used = time(NULL);

		printf("save token\n");
		TokensFile->Write(&token,token_addr,sizeof(Token));

		printf("commit\n");
		Transactions->Commit();

		UnlockWrite();

		printf("response\n");
		HTTP_ResponseHeaders(client,"200 OK",NULL);
		SendSetCookie(client,token_id,token.token);
		HTTP_SendNothing(client);
	}
	else
	{
		HTTP_ResponseHeaders(client,"403 Forbidden","text/plain");
		HTTP_SendStringContent(client,"Invalid username or password.");
	};
};

Sint64 InterpretTokenID (char* token_cookie)
{
	size_t cookie_len = strlen(token_cookie);
	if (cookie_len != TOKEN_FULL_LENGTH) return -1;

	Uint64 token_id = 0;
	for (int i = 0; i < TOKEN_IDSTR_LENGTH; i++)
	{
		char value = FromBase64(token_cookie[i]);
		if (value >= 64) return -1; // invalid Base64 char
		token_id = (token_id << 6) | value;
	};

	return token_id;
};

void RefreshTokenExpiration (Sint64 token_id) /// Low level. Activates locks.
{
	LockWrite();

	Token token; // Careful, part of this will become stale as the linked lists are manipulated.
	TokensFile->Read(&token,sizeof(TokensHeader) + sizeof(Token) * token_id,sizeof(Token));

	// Tiny chance the token was pruned before LockWrite acquires exclusive access.
	if (token.account_id >= 0)
	{
		// Also a chance it was pruned *and added to another account* but this will just refresh the token for its new account.

		if (token.next_active_id >= 0)
		{
			TokenGlobalActiveRemove(&token);
			TokenGlobalActiveAdd(token_id);
		};
		if (token.next_user_token >= 0)
		{
			AccountTokensRemove(&token,token.account_id);
			AccountTokensAdd(token_id,token.account_id);
		};

		time_t last_used = time(NULL);
		TokensFile->Write(&last_used,sizeof(TokensHeader) + sizeof(Token) * token_id + offsetof(Token,last_used),sizeof(time_t));

		Transactions->Commit();
	};

	UnlockWrite();
};

Sint64 TokenUser (Sint64 token_id, char* token_cookie) /// Low level. Activates locks.
{
	PreventWrite();

	// Load header from disk.
	TokensHeader header;
	TokensFile->Read(&header,0,sizeof(TokensHeader));
	if (token_id >= header.current_capacity) return -1; // Out of range.

	// Load token from disk.
	Token token;
	TokensFile->Read(&token,sizeof(TokensHeader) + sizeof(Token) * token_id,sizeof(Token));

	AllowWrite();

	if (token.account_id < 0) return -1; // Not an active token.

	for (int i = 0; i < TOKEN_DATA_LENGTH; i++)
		if (token_cookie[TOKEN_IDSTR_LENGTH + i] != token.token[i])
			return -1; // Token content mismatch.

	if (token.last_used + TOKEN_REFRESH_RESOLUTION < time(NULL))
		RefreshTokenExpiration(token_id);

	return token.account_id; // Valid
};

Sint64 CurrentUser (Client* client) /// High level.
{
	char* token_cookie = GetCookie(client,SMHAUTH_COOKIE_NAME);
	if (!token_cookie) return -1;

	Sint64 token_id = InterpretTokenID(token_cookie);
	if (token_id < 0) return -1;

	Sint64 account_id = TokenUser(token_id,token_cookie);

	return account_id;
};

void LogOut (Client* client) /// High level.
{
	char* token_cookie = GetCookie(client,SMHAUTH_COOKIE_NAME);
	if (!token_cookie) return -1;

	Sint64 token_id = InterpretTokenID(token_cookie);
	if (token_id < 0) return -1;

	printf("look up token user\n");
	Sint64 account_id = TokenUser(token_id,token_cookie);
	printf("token user is %lld\n",account_id);

	if (account_id >= 0)
	{
		LockWrite();

		Token token;
		TokensFile->Read(&token,sizeof(TokensHeader) + sizeof(Token) * token_id,sizeof(Token));

		if (token.account_id == account_id) // Verify it didn't get deactivated while acquiring the write-lock.
		{
			printf("deactivating...\n");
			DeactivateToken(token_id);
			printf("successfully deactivated\n");
		};

		Transactions->Commit();

		UnlockWrite();

		HTTP_ResponseHeaders(client,"200 OK",NULL);
		HTTP_SendNothing(client);
	}
	else
	{
		HTTP_ResponseHeaders(client,"403 Forbidden","text/plain");
		HTTP_SendStringContent(client,"Invalid token.");
	};
};



Sint64 NewAccount (UserPassPair userpass, char** failure_message)
{
	///sodium_mlock(userpass.password,userpass.passlen);

	*failure_message = ValidateUsername(userpass.username);
	if (*failure_message) return -1;
	*failure_message = ValidatePassword(userpass.password,userpass.passlen);
	if (*failure_message) return -1;

	Account account;
	account.by_name_left = -1;
	account.by_name_right = -1;
	account.first_user_token = -1;
	account.last_user_token = -1;
	account.n_user_tokens = 0;
	// account.name_addr assigned later
	account.name_len = strlen(userpass.username);
	// account.pass_salt:
	randombytes_buf(account.pass_salt,crypto_pwhash_SALTBYTES);
	// account.pass_hash:
	int hash_status =
		crypto_pwhash(
			account.pass_hash,SMHAUTH_PWHASH_LEN,
			userpass.password,userpass.passlen,
			account.pass_salt,
			crypto_pwhash_OPSLIMIT_MODERATE,crypto_pwhash_MEMLIMIT_MODERATE,
			crypto_pwhash_ALG_DEFAULT
		);

	///sodium_munlock(userpass.password,userpass.passlen);

	if (hash_status) // nonzero means failed
	{
		printf("ERROR: hash computation failure (during account creation)\n");
		*failure_message = "Hashing failed.";
		return -1;
	};

	Sint64 account_id;

	LockWrite();

	size_t by_name_downptr;
	Sint64 existing = LookupUsername(userpass.username,&by_name_downptr);
	if (existing >= 0) // Only create account if this username isn't taken.
	{
		printf("Account \"%s\" already exists.\n",userpass.username);
		*failure_message = "Account already exists.";
		account_id = -1;
	}
	else
	{
		AccountsHeader header;
		AccountsFile->Read(&header,0,sizeof(AccountsHeader));
		account_id = header.n_accounts++;
		AccountsFile->Write(&header,0,sizeof(AccountsHeader));

		//printf("allocating name\n");
		account.name_addr = NamesAllocator->Allocate(account.name_len);
		//printf("name allocated\n");
		NamesFile->Write(userpass.username,account.name_addr,account.name_len);
		//printf("name written\n");

		// write the account structure
		AccountsFile->Write(&account,sizeof(AccountsHeader) + sizeof(Account) * account_id,sizeof(Account));

		// add the new account to the by-name BST
		AccountsFile->Write(&account_id,by_name_downptr,8);

		Transactions->Commit();
	};

	UnlockWrite();

	return account_id;
};



void Prune () /// Does not activate locks.
{
	time_t timenow = time(NULL);

	while (1)
	{
		TokensHeader header;
		TokensFile->Read(&header,0,sizeof(TokensHeader));

		if (header.first_active_id < 0) break;

		Token token;
		TokensFile->Read(&token,sizeof(TokensHeader) + sizeof(Token) * header.first_active_id,sizeof(Token));

		if (token.last_used + TOKEN_MAX_AGE >= timenow) break;

		DeactivateToken(header.first_active_id);

		Transactions->Commit();
	};
};

void CleanUpTokens ()
{
	while (!ServerShouldDie)
	{
		LockWrite();
		Prune();
		UnlockWrite();
		time_t wait_until = time(NULL) + PruningInterval;
		while (time(NULL) < wait_until)
		{
			if (ServerShouldDie) break;
			SDL_Delay(DeathCheckInterval);
		};
	};
};

SDL_Thread* TokenCleanupThread;

int TokenAuthInit ()
{
	WritePrevention = SDL_CreateMutex();
	WriteLock = SDL_CreateMutex();
	if (!WritePrevention || !WriteLock) return -1;

	if (sodium_init() < 0) return -2;

	try
	{
	    RawFile = new BasicFile("tokenauth.tf",true);
	    Transactions = new TransactionalFile(RawFile);
		TokensAccountsHost = new ArraysFileHost(Transactions,3);
	    ///TokensAccountsHost = new ArraysFileHost(new BasicFile("raw"),3);///
        AccountsFile = TokensAccountsHost->arrays + 0;
	    TokensFile = TokensAccountsHost->arrays + 1;
	    NamesFile = TokensAccountsHost->arrays + 2;
	    NamesAllocator = new AllocatingFile(NamesFile);
	    if (TokensFile->CurrentSize() < sizeof(TokensHeader) || AccountsFile->CurrentSize() < sizeof(AccountsHeader))
		{
			printf("Writing new TokenAuth headers.\n");

			TokensHeader tokens;
			tokens.current_capacity = 0;
			tokens.first_active_id = -1;
			tokens.last_active_id = -1;
			tokens.first_available_id = -1;
			TokensFile->Write(&tokens,0,sizeof(TokensHeader));

			AccountsHeader accounts;
			accounts.by_name_root = -1;
			accounts.n_accounts = 0;
			AccountsFile->Write(&accounts,0,sizeof(AccountsHeader));

			Transactions->Commit();
		}
		else printf("TokenAuth headers already exist.\n");
	}
	catch (std::runtime_error error)
	{
	    printf("Failure to initialize crucial file objects: \"%s\"\n",error.what());
	    return -3;
	};

	TokenCleanupThread = SDL_CreateThread(CleanUpTokens,"SMH tokenauth pruning",NULL);
	if (!TokenCleanupThread) return -4;

	return 0;
};

void TokenAuthQuit ()
{
	SDL_WaitThread(TokenCleanupThread,NULL);
	delete NamesAllocator;
	delete TokensAccountsHost;
	delete Transactions;
	delete RawFile;
	SDL_DestroyMutex(WritePrevention);
	SDL_DestroyMutex(WriteLock);
};
