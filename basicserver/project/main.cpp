#include <smh/smh.h>
#include <mime.h>
#define SDL_main main

extern volatile char ServerShouldDie;
extern SDL_atomic_t ListenersActive;

char* alloc_strcat (char* str1, char* str2)
{
	size_t len1 = strlen(str1);
	size_t len2 = strlen(str2);
	char* out = malloc(len1 + len2 + 1);
	memcpy(out,str1,len1);
	memcpy(out + len1,str2,len2);
	out[len1 + len2] = 0;
	return out;
};

void HandleRequest (Client* client)
{
	if (!strcmp(client->method,"GET"))
	{
		char* fullpath = alloc_strcat("DocumentRoot/",client->path);

		FILE* file = fopen(fullpath,"rb");
		if (file)
		{
			Status200(client,DeduceMimeType(fullpath));
			HTTP_SendFile(client,file);
		}
		else Error404(client);

		free(fullpath);
	}
	else if (!strcmp(client->method,"POST"))
	{
		DiscardPostData(client);
		Error404(client);
	}
	else
	{
		HTTP_ResponseHeaders(client,"405 Method Not Allowed","text/html");
		SendHeader(client,"Allow","GET, POST");
		HTTP_SendStringContent(client,"server does not recognize the request's http method");
	};
};



int main ()
{
	if (HTTP_Init()) return -1;
	StartListening(80,ServeClient_HTTP,HandleRequest);

	while (1)
	{
		SDL_Delay(DeathCheckInterval);
		if (!SDL_AtomicGet(&ListenersActive)) ServerShouldDie = 1;
		if (ServerShouldDie)
		{
			printf("Shut down signal received.\n");
			break;
		};
	};

	HTTP_Quit();

	printf("Clean exit!\n");
	return 0;
};
