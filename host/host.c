
#include "host.h"
void HandleByHost (Client* client, HostMapping* hostmappings /* terminated by an entry with host_name==NULL */)
{
	//printf("method: \"%s\"\n",client->method);
	//printf("path: \"%s\"\n",client->path);

	char* host_value = GetHeader(client,"Host");
	//printf("host value is \"%s\"\n",host_value);
	if (!host_value) host_value = "";
	HostMapping* mapping = hostmappings;
	while (mapping->host_name)
	{
		if (!strcmp(host_value,mapping->host_name)) break;
		mapping++;
	};
	mapping->handle_request(client);
};



