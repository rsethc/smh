INCLUDES += -I.
INCLUDES += -I/usr/include -I/usr/local/include
CFLAGS += -w -O2 -fomit-frame-pointer -s -fPIC -w
CXXFLAGS += $(CFLAGS) -fpermissive

NAMEVER = package

PRODUCT_LIBS =\
	libsmh.a\
	libhost.a\
	libmime.a\
	libtokenauth.a\

PRODUCT_EXECUTABLES =\
	basicserver/basicserver\
	exampleserver/exampleserver\
	pythonserver/pythonserver\

LINK_LIBS =\
 	-L.\
	-L/lib\
	-L/usr/lib\
	-L/usr/local/lib\
	-ltokenauth\
	-llio\
	-lmime\
	-lhost\
	-lsmh\
	-lsodium\
	-lSDL2_net\
	-lSDL2\
	-ldl\

ALL_PRODUCTS = $(PRODUCT_LIBS) $(PRODUCT_EXECUTABLES)

LIBSMH_OBJS = $(patsubst %.c,%.o,$(shell find smh -iname '*.c'))
LIBHOST_OBJS = $(patsubst %.c,%.o,$(shell find host -iname '*.c'))
LIBMIME_OBJS = $(patsubst %.c,%.o,$(shell find mime -iname '*.c'))
LIBTOKENAUTH_OBJS = $(patsubst %.cpp,%.o,$(shell find tokenauth -iname '*.cpp'))
BASICSERVER_OBJS = $(patsubst %.cpp,%.o,$(shell find basicserver/project -iname '*.cpp'))
EXAMPLESERVER_OBJS = $(patsubst %.cpp,%.o,$(shell find exampleserver/exampleserver-project -iname '*.cpp'))
PYTHONSERVER_OBJS = $(patsubst %.c,%.o,$(shell find pythonserver/pythonserver-project -iname '*.c'))

all: $(ALL_PRODUCTS)

install: all
	install $(PRODUCT_LIBS) /usr/local/lib
	install $(shell find -iname '*.h') /usr/local/include
	install $(shell find pythonserver/py_smh_api -iname '*.py') $(shell python -m site --user-base)/lib/python2.7/site-packages

libsmh.a: $(LIBSMH_OBJS)
	ar -cr $@ $^

libhost.a: $(LIBHOST_OBJS)
	ar -cr $@ $^

libmime.a: $(LIBMIME_OBJS)
	ar -cr $@ $^

libtokenauth.a: $(LIBTOKENAUTH_OBJS)
	ar -cr $@ $^

basicserver/basicserver: $(BASICSERVER_OBJS)
	g++ -o $@ $^ $(LINK_LIBS)

exampleserver/exampleserver: $(EXAMPLESERVER_OBJS)
	g++ -o $@ $^ $(LINK_LIBS)

pythonserver/pythonserver: $(PYTHONSERVER_OBJS)
	g++ -o $@ $^ $(LINK_LIBS)

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $(INCLUDES) $< -o $@

%.o: %.c
	$(CC) -c $(CFLAGS) $(INCLUDES) $< -o $@

clean:
	@rm -rvf $(shell find . -iname '*.o') *.a
