#include <smh/smh.h>
#include <tokenauth.h>
#include <mime.h>
#define SDL_main main

#include <time.h>



extern SDL_atomic_t ListenersActive;
extern volatile char ServerShouldDie;
extern volatile time_t InitializedAtTime;
extern Uint64 RequestsEverServed;
extern Uint64 RequestsServedLastSecond;
extern SDL_atomic_t OpenConnections;

char* alloc_strcat (char* str1, char* str2)
{
	size_t len1 = strlen(str1);
	size_t len2 = strlen(str2);
	char* out = malloc(len1 + len2 + 1);
	memcpy(out,str1,len1);
	memcpy(out + len1,str2,len2);
	out[len1 + len2] = 0;
	return out;
};
void Pull (Client* client)
{
    printf("PULL REQUEST\n");
    HTTP_ResponseHeaders(client,"404 woah buster this isn't implemented yet",NULL);
    SendEmpty(client);
};
void HandleAdmin (Client* client)
{
	//printf("admin...\n");
	FILE* file;
	if (!strcmp(client->path,"admin/shutdown"))
	{
		ServerShouldDie = 1;
		Status200(client,NULL);
		SendEmpty(client);
	}
	else if (!strcmp(client->path,"admin/pull"))
    {
        Pull(client);
    }
	else if (!strcmp(client->path,"admin/stats"))
	{
		time_t timenow = time(NULL);
		int seconds = timenow - InitializedAtTime;
		int minutes = seconds / 60;
		int hours = minutes / 60;
		int days = hours / 24;
		seconds -= minutes * 60;
		minutes -= hours * 60;
		hours -= days * 24;

		char* uptime_str;
		//asprintf(&str,"%u",SDL_GetTicks() / 1000);
		asprintf(&uptime_str,"%d days, %d hours, %d minutes, %d seconds",days,hours,minutes,seconds);



		char* requests_ever_str;
		asprintf(&requests_ever_str,"%llu",RequestsEverServed);



		char* active_conns_str;
		asprintf(&active_conns_str,"%u",SDL_AtomicGet(&OpenConnections));



		char* requests_last_second_str;
		asprintf(&requests_last_second_str,"%llu",RequestsServedLastSecond);



		char* full_response_str;
		asprintf(&full_response_str,"uptime=%s;served=%s;active-connections=%s;served-last-second=%s",uptime_str,requests_ever_str,active_conns_str,requests_last_second_str);



		Status200(client,"text/plain");
		HTTP_SendStringContent(client,full_response_str);

		free(full_response_str);
		free(uptime_str);
		free(requests_ever_str);
		free(active_conns_str);
		free(requests_last_second_str);
	}
	else
	{
		char* fullpath = alloc_strcat("AdminPages/",client->path + strlen("admin/"));
		file = fopen(fullpath,"rb");
		free(fullpath);
		if (file)
		{
			Status200(client,DeduceMimeType(client->path));
			HTTP_SendFile(client,file);
		}
		else
		{
			Error404(client);
		};
	};

};



void HandleRequest (Client* client)
{
	if (!strcmp(client->method,"GET"))
	{
		if (StartsWith(client->path,"admin/"))
        {
            if (CurrentUser(client) >= 0) HandleAdmin(client);
            else
            {
                HTTP_ResponseHeaders(client,"403 Forbidden","text/html");
                HTTP_SendStringContent(client,"Log in first, to view this content.");
            };
        }
		else
		{
			if (StartsWith(client->path,"restricted/"))
			{
				if (CurrentUser(client) < 0)
				{
					HTTP_ResponseHeaders(client,"403 Forbidden","text/html");
					HTTP_SendStringContent(client,"Log in first, to view this content.");
					return;
				};
			};
			printf("generic request\n");

			char* fullpath = alloc_strcat("DocumentRoot/",client->path);

			FILE* file = fopen(fullpath,"rb");
			if (file)
			{
				Status200(client,DeduceMimeType(fullpath));
				HTTP_SendFile(client,file);
			}
			else
			{
				Error404(client);
			};

			free(fullpath);
		};
	}
	else if (!strcmp(client->method,"POST"))
	{
		if (!strcmp(client->path,"login"))
		{
			LogIn(client);
		}
		else if (!strcmp(client->path,"logout"))
		{
			LogOut(client);
		}
		else if (!strcmp(client->path,"post_test"))
		{
			PostData data = GetPostData(client,1024);
			Status200(client,"text/plain");
			for (int i = 0; i < data.length; i++)
			{
				if (data.buf[i] == ' ') data.buf[i] = '$';
				else data.buf[i]++;
			};
			HTTP_SendBinaryContent(client,data.buf,data.length);
		}
		else if (!strcmp(client->path,"authorized_post_test"))
		{
			PostData data = GetPostData(client,1024);
			if (CurrentUser(client) >= 0)
			{
				Status200(client,"text/plain");
				for (int i = 0; i < data.length; i++)
				{
					if (data.buf[i] == ' ') data.buf[i] = '$';
					else data.buf[i]++;
				};
				HTTP_SendBinaryContent(client,data.buf,data.length);
			}
			else
			{
				HTTP_ResponseHeaders(client,"403 Forbidden","text/html");
				HTTP_SendStringContent(client,"Log in first, to view this content.");
			};
		}
		else
		{
			DiscardPostData(client);
			Error404(client);
		};
	}
	else
	{
		HTTP_ResponseHeaders(client,"405 Method Not Allowed","text/html");
		SendHeader(client,"Allow","GET, POST");
		HTTP_SendStringContent(client,"server does not recognize the request's http method");
	};
};



int main ()
{
	if (HTTP_Init()) return -1;
	if (TokenAuthInit()) return -2;

	char* newacc_error;
	UserPassPair newacc_userpass = {"username","password",8};
	NewAccount(newacc_userpass,&newacc_error);
	if (newacc_error) printf("Can't create account because: \"%s\"\n",newacc_error);

	StartListening(80,ServeClient_HTTP,HandleRequest);

	while (1)
	{
		SDL_Delay(DeathCheckInterval);
		if (!SDL_AtomicGet(&ListenersActive)) ServerShouldDie = 1;
		if (ServerShouldDie)
		{
			printf("Shut down signal received.\n");
			break;
		};
		SMH_StatsStep();
	};

	HTTP_Quit();
	TokenAuthQuit(); // Don't do this until all active connections are closed!

	printf("Clean exit!\n");
	return 0;
};
