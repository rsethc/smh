Method = None
InputFile = None 
OutputFile = None 
HeadersFile = None 
StatusFile = None 
import sys
for arg in sys.argv: 
	name = arg # If '=' is not found default to the entire arg being the name.
	value = None # If '=' is not found default to no value.
	pieces = arg.split('=',1) # Only split on the first '='.
	if (len(pieces) == 2):
		# Did find '=' to split on.
		name = pieces[0]
		value = pieces[1]
		# Remove quotation marks around the value, if they are present.
		if len(value) >= 2:
			if (value[0] == '"' and value[-1] == '"'):
				value = value[1:-1]
	if name == "--output-file":
		OutputFile = open(value,"wb")
	elif name == "--input-file":
		InputFile = open(value,"r")
	elif name == "--method":
		Method = value 
	elif name == "--headers-file":
		HeadersFile = open(value,"wb")
	elif name == "--status-file":
		StatusFile = open(value,"wb")
if (OutputFile == None or InputFile == None or Method == None or HeadersFile == None or StatusFile == None):
	print("Did not receive expected command-line arguments.")
	sys.exit(-1)
def header (h):
	HeadersFile.write(str(h).encode("ascii"))
	HeadersFile.write(  "\n".encode("ascii"))
def write (w):
	OutputFile.write( str(w).encode("ascii"))
def writeln (w):
	OutputFile.write( str(w).encode("ascii"))
	OutputFile.write( "\r\n".encode("ascii"))
def status (s):
	StatusFile.seek(0)
	StatusFile.write( str(s).encode("ascii"))
status("200 OK") # Default status.
exit = sys.exit 	
