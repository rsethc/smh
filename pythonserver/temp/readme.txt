This directory is where files for I/O between SMH and Python will be placed during request serving. 
The server should automatically remove these temporary files during normal operation.