#include <smh/smh.h>
#include <tokenauth.h>
#define SDL_main main

#include <time.h>



char* alloc_strcat (char* str1, char* str2)
{
	size_t len1 = strlen(str1);
	size_t len2 = strlen(str2);
	char* out = malloc(len1 + len2 + 1);
	memcpy(out,str1,len1);
	memcpy(out + len1,str2,len2);
	out[len1 + len2] = 0;
	return out;
};

#define MAX_SCRIPT_INPUT (16*1024*1024) // 16 MiB

SDL_atomic_t PythonRunID = {0};

#define OUTPUT_ERR

char* LoadFile (char* path)
{
	FILE* file = fopen(path,"rb");
	if (!file) return NULL;
	fseek(file,0,SEEK_END);
	size_t len = ftell(file);
	fseek(file,0,SEEK_SET);
	char* out = malloc(len + 1);
	fread(out,1,len,file);
	out[len] = 0;
	fclose(file);
	return out;
};

void RunPythonScript (char* path, Client* client)
{
	int run_id = SDL_AtomicAdd(&PythonRunID,1);

	char* in_name;
	asprintf(&in_name,"temp/in_%X.txt",run_id);
	char* out_name;
	asprintf(&out_name,"temp/out_%X.txt",run_id);
	char* head_name;
	asprintf(&head_name,"temp/head_%X.txt",run_id);
	char* status_name;
	asprintf(&status_name,"temp/status_%X.txt",run_id);
	#ifdef OUTPUT_ERR
	char* console_name;
	asprintf(&console_name,"temp/console_%X.txt",run_id);
	char* err_name;
	asprintf(&err_name,"temp/err_%X.txt",run_id);
	#endif

	printf("in path: %s\nout path: %s\nhead path: %s\nstatus path: %s\n",in_name,out_name,head_name,status_name);
	#ifdef OUTPUT_ERR
	printf("output path: %s\nerr path: %s\n",console_name,err_name);
	#endif

	FILE* in_file = fopen(in_name,"wb");
	if (!in_file)
	{
		HTTP_ResponseHeaders(client,"500 Internal Server Error","text/html");
		HTTP_SendStringContent(client,"Failed to provide script input.");
	}
	else
	{
		if (!strcmp(client->method,"POST"))
		{
			PostData postdata = GetPostData(client,MAX_SCRIPT_INPUT);
			fwrite(postdata.buf,1,postdata.length,in_file);
		};
		fclose(in_file);

		char* cmd;
		#ifdef OUTPUT_ERR
		asprintf(
			&cmd,
			"python 1> \"%s\" 2> \"%s\" \"%s\" --input-file=\"%s\" --output-file=\"%s\" --method=\"%s\" --headers-file=\"%s\" --status-file=\"%s\"",
			console_name,err_name,path,in_name,out_name,client->method,head_name,status_name
		);
		#else
		asprintf(
			&cmd,
			"python \"%s\" --input-file=\"%s\" --output-file=\"%s\" --method=\"%s\" --headers-file=\"%s\" --status-file=\"%s\"",
			path,in_name,out_name,client->method,head_name,status_name
		);
		#endif
		//asprintf(&cmd,"python %s > %s",path,out_name);
		printf("executing {%s}\n",cmd);
		int result = system(cmd);
		//printf("execution result is %d\n",result);
		free(cmd);
		if (result == 0)
		{
			FILE* status = LoadFile(status_name);
			char free_status = !!status;
			if (!status) status = "200 OK";
			HTTP_ResponseHeaders(client,status,NULL); // Let script set its own MIME type.
			if (free_status) free(status);

			FILE* head_file = fopen(head_name,"rb");
			if (head_file)
			{
				fseek(head_file,0,SEEK_END);
				size_t len = ftell(head_file);
				fseek(head_file,0,SEEK_SET);
				EmitFile(client,head_file,len);
				fclose(head_file);
			};

			FILE* out_file = fopen(out_name,"rb");
			if (out_file)
			{
				HTTP_SendFile(client,out_file);
				fclose(out_file);
			}
			else HTTP_SendNothing(client);
		}
		else
		{
			HTTP_ResponseHeaders(client,"500 Internal Server Error","text/html");
			#ifdef OUTPUT_ERR

			char* console = LoadFile(console_name);
			char free_console = !!console;
			if (!console) console = "(Failed to retrieve stdout.)";

			char* err = LoadFile(err_name);
			char free_err = !!err;
			if (!err) err = "(Failed to retrieve stderr.)";

			char* headers = LoadFile(head_name);
			char free_headers = !!headers;
			if (!headers) headers = "(Failed to retrieve headers.)";

			char* document = LoadFile(out_name);
			char free_document = !!document;
			if (!document) document = "(Failed to retrieve document.)";

			char* full;
			asprintf(&full,
				"<h1>Failed to execute script.</h1><hr>"
				"<h2>stdout:</h2><pre>%s</pre><hr>"
				"<h2>stderr:</h2><pre>%s</pre><hr>"
				"<h2>Headers:</h2><pre>%s</pre><hr>"
				"<h2>Document:</h2>%s",
				console,err,headers,document
			);
			if (free_console) free(console);
			if (free_err) free(err);
			if (free_headers) free(headers);
			if (free_document) free(document);
			HTTP_SendStringContent(client,full);
			free(full);
			#else
			HTTP_SendStringContent(client,"Failed to execute script.");
			#endif
		};
		remove(in_name);
		remove(out_name);
		remove(head_name);
		remove(status_name);
		#ifdef OUTPUT_ERR
		remove(console_name);
		remove(err_name);
		#endif
	};
	free(in_name);
	free(out_name);
	free(head_name);
	free(status_name);
	#ifdef OUTPUT_ERR
	free(console_name);
	free(err_name);
	#endif
};
void HandleWithPython (Client* client)
{
	char* fullpath = alloc_strcat("document-root/",client->path);
	if (EndsWith(client->path,".py"))
	{
		FILE* scriptfile = fopen(fullpath,"rb");
		if (scriptfile)
		{
			fclose(scriptfile);
			RunPythonScript(fullpath,client); // handles GET and POST internally
		}
		else
		{
			Error404(client);
		};
	}
	else if (!strcmp(client->method,"GET"))
	{
		char* mimetype = "text/html";
		if (EndsWith(client->path,".src"))
		{
			fullpath[strlen(fullpath) - 4] = 0;
			// "/test.py.src" -> "test.py"
			// And then proceed to serve the file without executing it.
			mimetype = "text/plain";
		};

		FILE* file = fopen(fullpath,"rb");
		if (file)
		{
			Status200(client,mimetype);
			//SendHeader(client,"Content-Type",mimetype);
			HTTP_SendFile(client,file);
		}
		else
		{
			Error404(client);
		};
	}
	else if (!strcmp(client->method,"POST"))
	{
		DiscardPostData(client);
		Error404(client);
	}
	else
	{
		HTTP_ResponseHeaders(client,"405 Method Not Allowed","text/html");
		SendHeader(client,"Allow","GET, POST");
		HTTP_SendStringContent(client,"server does not recognize the request's http method");
	};
	free(fullpath);
};

#include <host.h>

HostMapping HostMappings [] =
{
	// The default case, for if nothing else matches Host header (perhaps because there isn't even a Host header at all).
	{NULL,HandleWithPython}
};

void HandleRequest (Client* client)
{
	HandleByHost(client,HostMappings);
};

extern SDL_atomic_t ListenersActive;
extern volatile char ServerShouldDie;

int main ()
{
	if (HTTP_Init()) return -1;



	StartListening(3001,ServeClient_HTTP,HandleRequest);
	while (1)
	{
		SDL_Delay(DeathCheckInterval);
		if (!SDL_AtomicGet(&ListenersActive)) ServerShouldDie = 1;
		if (ServerShouldDie)
		{
			printf("Shut down signal received.\n");
			break;
		};
		SMH_StatsStep();
	};



	HTTP_Quit();
	printf("Clean exit!\n");
	return 0;
};
