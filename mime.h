#ifndef MIME_H_INCLUDED
#define MIME_H_INCLUDED

/// C function definitions for C++
#ifdef __cplusplus
extern "C" {
#endif



typedef struct
{
	char* extension;
	char* mimetype;
}
MimeMapping;

char* DeduceMimeType (char* path);



/// C function definitions for C++
#ifdef __cplusplus
}
#endif

#endif // MIME_H_INCLUDED
