#include <smh/smh.h>

#include <mime.h>

MimeMapping MimeMap [] =
{
	// HTML
	{".html","text/html"},
	{".htm","text/html"},
	{".xhtml","text/html"},

	// Images
	{".png","image/png"},
	{".bmp","image/bmp"},
	{".jpg","image/jpeg"},
	{".jpeg","image/jpeg"},

	// Default
	{NULL,"text/plain"}
};
int MimeMapLen = sizeof(MimeMap) / sizeof(*MimeMap);

char* DeduceMimeType (char* path)
{
	for (int i = 0; i < MimeMapLen; i++)
	{
		MimeMapping mapping = MimeMap[i];
		if (mapping.extension) if (!EndsWith(path,mapping.extension)) continue;
		return mapping.mimetype;
	};
};
