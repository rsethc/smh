#include <smh/smh.h>
#include <tokenauth.h>
#include <mime.h>
#define SDL_main main

#include <time.h>



extern SDL_atomic_t ListenersActive;
extern volatile char ServerShouldDie;
extern volatile time_t InitializedAtTime;
extern Uint64 RequestsEverServed;
extern Uint64 RequestsServedLastSecond;
extern SDL_atomic_t OpenConnections;

char* alloc_strcat (char* str1, char* str2)
{
	size_t len1 = strlen(str1);
	size_t len2 = strlen(str2);
	char* out = malloc(len1 + len2 + 1);
	memcpy(out,str1,len1);
	memcpy(out + len1,str2,len2);
	out[len1 + len2] = 0;
	return out;
};
void HandleAdmin (Client* client)
{
	//printf("admin...\n");
	FILE* file;
	if (!strcmp(client->path,"admin/shutdown"))
	{
		ServerShouldDie = 1;
		Status200(client,NULL);
		SendEmpty(client);
	}
	else if (!strcmp(client->path,"admin/stats"))
	{
		time_t timenow = time(NULL);
		int seconds = timenow - InitializedAtTime;
		int minutes = seconds / 60;
		int hours = minutes / 60;
		int days = hours / 24;
		seconds -= minutes * 60;
		minutes -= hours * 60;
		hours -= days * 24;

		char* uptime_str;
		//asprintf(&str,"%u",SDL_GetTicks() / 1000);
		asprintf(&uptime_str,"%d days, %d hours, %d minutes, %d seconds",days,hours,minutes,seconds);



		char* requests_ever_str;
		asprintf(&requests_ever_str,"%llu",RequestsEverServed);



		char* active_conns_str;
		asprintf(&active_conns_str,"%u",SDL_AtomicGet(&OpenConnections));



		char* requests_last_second_str;
		asprintf(&requests_last_second_str,"%llu",RequestsServedLastSecond);



		char* full_response_str;
		asprintf(&full_response_str,"uptime=%s;served=%s;active-connections=%s;served-last-second=%s",uptime_str,requests_ever_str,active_conns_str,requests_last_second_str);



		Status200(client,"text/plain");
		HTTP_SendStringContent(client,full_response_str);

		free(full_response_str);
		free(uptime_str);
		free(requests_ever_str);
		free(active_conns_str);
		free(requests_last_second_str);
	}
	else
	{
		char* fullpath = alloc_strcat("AdminPages/",client->path + strlen("admin/"));
		file = fopen(fullpath,"rb");
		free(fullpath);
		if (file)
		{
			Status200(client,DeduceMimeType(client->path));
			HTTP_SendFile(client,file);
		}
		else
		{
			Error404(client);
		};
	};

};



void Deny (Client* client) // generic 403 response
{
	printf("refusing access\n");
	HTTP_ResponseHeaders(client,"403 Forbidden","text/html");
	HTTP_SendStringContent(client,"Log in first, to view this content.");
};

void HandleSMHTest (Client* client)
{
	if (!strcmp(client->method,"GET"))
	{
		if (!*client->path) client->path = "index.html";

		if (StartsWith(client->path,"admin/"))
		{
			if (CurrentUser(client) >= 0) HandleAdmin(client);
			else Deny(client);
		}
		else
		{
			printf("generic request\n");
			if (StartsWith(client->path,"restricted/"))
			{
				if (CurrentUser(client) < 0)
				{
					Deny(client);
					return;
				};
				printf("restricted, but access granted\n");
			};

			char* fullpath = alloc_strcat("Root_SMHTest/",client->path);

			FILE* file = fopen(fullpath,"rb");
			if (file)
			{
				Status200(client,DeduceMimeType(fullpath));
				HTTP_SendFile(client,file);
			}
			else
			{
				Error404(client);
			};

			free(fullpath);
		};
	}
	else if (!strcmp(client->method,"POST"))
	{
		if (!strcmp(client->path,"login"))
		{
			LogIn(client);
		}
		else if (!strcmp(client->path,"logout"))
		{
			LogOut(client);
		}
		else if (!strcmp(client->path,"post_test"))
		{
			PostData data = GetPostData(client,1024);
			Status200(client,"text/plain");
			for (int i = 0; i < data.length; i++)
			{
				if (data.buf[i] == ' ') data.buf[i] = '$';
				else data.buf[i]++;
			};
			HTTP_SendBinaryContent(client,data.buf,data.length);
		}
		else if (!strcmp(client->path,"authorized_post_test"))
		{
			PostData data = GetPostData(client,1024);
			if (CurrentUser(client) >= 0)
			{
				Status200(client,"text/plain");
				for (int i = 0; i < data.length; i++)
				{
					if (data.buf[i] == ' ') data.buf[i] = '$';
					else data.buf[i]++;
				};
				HTTP_SendBinaryContent(client,data.buf,data.length);
			}
			else
			{
				HTTP_ResponseHeaders(client,"403 Forbidden","text/html");
				HTTP_SendStringContent(client,"Log in first, to view this content.");
			};
		}
		else
		{
			DiscardPostData(client);
			Error404(client);
		};
	}
	else
	{
		HTTP_ResponseHeaders(client,"405 Method Not Allowed","text/html");
		SendHeader(client,"Allow","GET, POST");
		HTTP_SendStringContent(client,"server does not recognize the request's http method");
	};
};
void HandleSMHTest2 (Client* client)
{
	if (!strcmp(client->method,"GET"))
	{
		if (!*client->path) client->path = "index.html";

		if (StartsWith(client->path,"admin/")) HandleAdmin(client);
		else
		{
			char* fullpath = alloc_strcat("Root_SMHTest2/",client->path);
			FILE* file = fopen(fullpath,"rb");
			free(fullpath);

			if (file)
			{
				Status200(client,DeduceMimeType(client->path));
				HTTP_SendFile(client,file);
			}
			else
			{
				Error404(client);
			};
		};
	}
	else if (!strcmp(client->method,"POST"))
	{
		DiscardPostData(client);
		Error404(client);
	}
	else
	{
		HTTP_ResponseHeaders(client,"405 Method Not Allowed","text/html");
		SendHeader(client,"Allow","GET, POST");
		HTTP_SendStringContent(client,"server does not recognize the request's http method");
	};
};

#include <host.h>
HostMapping HostMappings [] =
{
	{"smhtest2.glassbladegames.com",HandleSMHTest2},
	{"smhtest2-local.glassbladegames.com",HandleSMHTest2},

	// The default case, for if nothing else matches Host header (perhaps because there isn't even a Host header at all).
	{NULL,HandleSMHTest}
};



void HandleRequest (Client* client)
{
	HandleByHost(client,HostMappings);
};



#define RecvBufLen 4096
void JustDumpData (Worker* worker, TCPsocket client)
{
	printf("New connection on 1234.\n");

	SDLNet_SocketSet socket_set = SDLNet_AllocSocketSet(1);
	SDLNet_AddSocket(socket_set,(SDLNet_GenericSocket)client);

	while (1)
	{
		if (!SDLNet_CheckSockets(socket_set,1000)) break;

		char incoming [RecvBufLen];
		int received = SDLNet_TCP_Recv(client,incoming,RecvBufLen);

		if (received <= 0) break;

		printf("Received %d bytes...\n",received);
		for (int i = 0; i < 64; i++) putchar('-');
		putchar('\n');
		for (int i = 0; i < received; i++) putchar(incoming[i]);
		putchar('\n');
		for (int i = 0; i < 64; i++) putchar('-');
		putchar('\n');
	};

	SDLNet_TCP_Close(client);
};

void MakeAccount (char* username)
{
	UserPassPair userpass;
	userpass.username = username;
	userpass.password = "password123!";
	userpass.passlen = strlen(userpass.password);
	char* newacc_error;
	NewAccount(userpass,&newacc_error);
	if (newacc_error) printf("================	Can't create account \"%s\" because: \"%s\"\n",username,newacc_error);
	else printf("~~~~~~~~~~~~~~~~	Created account \"%s\".\n",username);
};
#define SpammerCount 0//10
void SpamLoop (int i)
{
	srand(time(NULL) + i);
	while (!ServerShouldDie)
	{
		char* username;
		asprintf(&username,"user%d",rand());
		MakeAccount(username);
		free(username);
	};
};

int main ()
{
	if (HTTP_Init()) return -1;
	if (TokenAuthInit()) return -2;

	StartListening(80,ServeClient_HTTP,HandleRequest);
	StartListening(1234,JustDumpData,NULL);

	SDL_Thread* spammers [SpammerCount];
	for (int i = 0; i < SpammerCount; i++)
		spammers[i] = SDL_CreateThread(SpamLoop,"Account Creation Spammer",i);

	MakeAccount("testuser");

	while (1)
	{
		SDL_Delay(DeathCheckInterval);
		if (!SDL_AtomicGet(&ListenersActive)) ServerShouldDie = 1;
		if (ServerShouldDie)
		{
			printf("Shut down signal received.\n");
			break;
		};
		SMH_StatsStep();
	};

	for (int i = 0; i < SpammerCount; i++)
		SDL_WaitThread(spammers[i],NULL);

	HTTP_Quit();
	TokenAuthQuit(); // Don't do this until all active connections are closed!

	printf("Clean exit!\n");
	return 0;
};
