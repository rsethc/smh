(function (){
	var code = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65];
	var accepted_position = 0;
	var prev_onkeydown = document.onkeydown;
	document.onkeydown = function (evt)
	{
		evt = evt || window.event; // Compatibility with dinosaurs.
		if (evt.keyCode == code[accepted_position])
		{
			if (++accepted_position == code.length) 
			{
				if (OnKonami) OnKonami();
				accepted_position = 0;
			};
		}
		else accepted_position = 0;
		if (prev_onkeydown) prev_onkeydown(evt); // Pass other events through to existing callback.
	};
})();