function PostSpamProc (url)
{
	var requests = 0;
	//var area = document.getElementById("area");
	var original_title = document.title;
	function OneSecond ()
	{
		var req = requests;
		requests = 0;
		//area.innerHTML = requests + " requests per second";
		//document.title = area.innerHTML;
		document.title = "[" + req + "] " + original_title;
		requests = 0;
	};
	var spammer = new Worker("postspam.js");
	spammer.postMessage(url);
	spammer.onmessage = function (msg)
	{
		requests += msg.data;
	};
	setInterval(OneSecond,1000);
};