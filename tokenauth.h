#ifndef TOKENS_H_INCLUDED
#define TOKENS_H_INCLUDED

#include <smh/smh.h>

#define TOKEN_DATA_LENGTH 150 // permutations of token content: 64 ^ TOKEN_DATA_LENGTH
#define TOKEN_IDSTR_LENGTH 10 // 6 bits per char (Base64) * 10 chars = 2^60 possibilities
#define TOKEN_FULL_LENGTH (TOKEN_DATA_LENGTH + TOKEN_IDSTR_LENGTH)
#define TOKENS_EXPANSION_AMOUNT 64 // number of positions to allocate when expanding the tokens array. must be >= 1.
#define MAX_ACCOUNT_TOKENS 10
#define SMHAUTH_COOKIE_NAME "smhauth_token"
#define SMHAUTH_PWHASH_LEN 1024//bytes
#define USERNAME_PASSWORD_SEPARATOR '\n'

// If #define'd, causes "Secure; " to be emitted in the token Set-Cookie HTTP header.
// This will prevent the browser from sending the token with any traffic that isn't HTTPS.
// You should only disable this feature if you do not support HTTPS at all!
///#define SMHAUTH_HTTPS_ONLY

#define TOKEN_MAX_AGE_STRING "604800"
#define TOKEN_MAX_AGE 604800 // Tokens expire if unused for 1 week (in seconds).
#define TOKEN_REFRESH_RESOLUTION (60*60) // If it's been less than an hour (in seconds) don't move the token around just because it's been accessed.
#define PruningInterval (60*60) // Hourly. Unit: seconds

#define MIN_USERNAME 3
#define MAX_USERNAME 60
#define MIN_PASSWORD 10


int AccountsInit (); /// INTERNAL. Non-zero return means failure.
void AccountsQuit (); /// INTERNAL.

int TokenAuthInit (); /// Non-zero return means failure.
void TokenAuthQuit ();
Sint64 CurrentUser (Client* client); /// returns -1 for no account
//#define IsLoggedIn(client) (CurrentUser(client) >= 0)
void LogIn (Client* client);
void LogOut (Client* client);

typedef struct
{
	char* username; // null-terminated

	char* password; // **NOT NULL-TERMINATED**
	size_t passlen; // length of password
}
UserPassPair;

Sint64 NewAccount (UserPassPair userpass, char** failure_message);
	/// returns -1 on failure, such as duplicate account maes
	/// regarding failure_message: Always pass a valid address even if you won't use the string. Do not free the string.

#endif // TOKENS_H_INCLUDED
