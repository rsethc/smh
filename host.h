#ifndef HOST_H_INCLUDED
#define HOST_H_INCLUDED

/// C function definitions for C++
#ifdef __cplusplus
extern "C" {
#endif



#include <smh/smh.h>

typedef struct
{
	char* host_name;
	void (*handle_request) (Client* client);
}
HostMapping;


void HandleByHost (Client* client, HostMapping* hostmappings /* terminated by an entry with host_name==NULL */);



/// C function definitions for C++
#ifdef __cplusplus
}
#endif

#endif // HOST_H_INCLUDED
