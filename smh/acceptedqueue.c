#include "acceptedqueue.h"



SocketAndFunction AcceptedSockets [AcceptQueueLen];
//SDL_mutex*
size_t AcceptedSocketReadPos = 0;
size_t AcceptedSocketWritePos = 0;
SDL_mutex* AcceptedSocketWritePosLock;
SDL_sem* AcceptedSocketsCount;
SDL_sem* AcceptedSocketsSpace;
void OnAcceptSocket (SocketAndFunction accepted)
{
	SDL_SemWait(AcceptedSocketsSpace);

	//printf("locking mutex: %llX\n",AcceptedSocketWritePosLock);
	SDL_LockMutex(AcceptedSocketWritePosLock);

	//printf("writing to %llu\n",AcceptedSocketWritePos);
	AcceptedSockets[AcceptedSocketWritePos] = accepted;
	//printf("placed %llX in queue\n",accepted);
	if (++AcceptedSocketWritePos >= AcceptQueueLen) AcceptedSocketWritePos = 0;

	SDL_UnlockMutex(AcceptedSocketWritePosLock);

	SDL_SemPost(AcceptedSocketsCount);
};








void AcceptedSocketsProc ()
{
	while (1)
	{
		SDL_SemWait(AcceptedSocketsCount);

		SocketAndFunction accepted = AcceptedSockets[AcceptedSocketReadPos];
		//printf("got socket %llX\n",accepted);
		if (++AcceptedSocketReadPos >= AcceptQueueLen) AcceptedSocketReadPos = 0;

		SDL_SemPost(AcceptedSocketsSpace);

		if (accepted.connection) StartWorker(accepted);
		else break;
	};
	//printf("exiting accepter\n");
	SDL_DestroySemaphore(AcceptedSocketsCount);
	SDL_DestroySemaphore(AcceptedSocketsSpace);
	SDL_DestroyMutex(AcceptedSocketWritePosLock);
};
void InitAccepter ()
{
	AcceptedSocketsCount = SDL_CreateSemaphore(0);
	AcceptedSocketsSpace = SDL_CreateSemaphore(AcceptQueueLen);
	AcceptedSocketWritePosLock = SDL_CreateMutex();
	SDL_DetachThread(SDL_CreateThread(AcceptedSocketsProc,"Accepted Sockets Manager",NULL));
};
void QuitAccepter () /// Must be called *after all listeners have exited* to avoid leaving connections open that were queued after the NULL was queued.
{
	SocketAndFunction null_entry;
	null_entry.connection = 0;
	OnAcceptSocket(null_entry);
};

