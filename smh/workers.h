#ifndef WORKERS_H_INCLUDED
#define WORKERS_H_INCLUDED

#include "smh.h"
typedef struct
{
	TCPsocket connection;
	void (*CommunicationFunction) (Worker* worker, TCPsocket client_socket, void* userdata);
	void* CommunicationFunctionUserdata;
}
SocketAndFunction;
struct Worker
{
	SDL_Thread* thread; // the thread for the worker

	SDL_sem* finish; // wait until the thread has finished before giving it new work
	SDL_sem* start; // signals the thread to start working on a new connection

	volatile char die_early;
	/** tells worker to die off if it has to stall for data,
		instead of waiting full length of the keep-alive timeout. **/

	Worker* next_stale;
	Worker* prev_stale;
	Worker* next_idle;
	// Don't need to store prev_idle since entries are only removed from the front of the Idle Queue.

	volatile SocketAndFunction socket_and_function;
};

void AddToStale (Worker* worker);
void RemoveFromStale (Worker* worker);



void StartWorker (SocketAndFunction connection);
void InitAllWorkers ();
void WaitAllWorkersExit ();



#endif // WORKERS_H_INCLUDED
