#ifndef ACCEPTEDQUEUE_H_INCLUDED
#define ACCEPTEDQUEUE_H_INCLUDED

#include "smh.h"
#include "workers.h"
void OnAcceptSocket (SocketAndFunction accepted);
void InitAccepter ();
void QuitAccepter (); /// Must be called *after all listeners have exited* to avoid leaving connections open that were queued after the NULL was queued.

#endif // ACCEPTEDQUEUE_H_INCLUDED
