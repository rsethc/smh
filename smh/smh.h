#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#define _GNU_SOURCE
#include <stdio.h>

#include <setjmp.h>

/// C function definitions for C++
#ifdef __cplusplus
extern "C" {
#endif



#define MinRecvLen 1024
#define MaxIdleMilliseconds 5000 // Keep-Alive Timeout
#define MaxMethodLen 32
#define MaxPathLen 2048 // High allowed length, to allow lots of URL variables.
#define MaxVersionLen 8 // "HTTP/1.0" "HTTP/1.1" "HTTP/2" - none exceed 8 characters.
#define MaxHeaderNameLen 128
#define MaxHeaderValueLen 16384
#define FileReadBufferSize 4096
#define AdminDieCommand "shutdown"
#define DocumentRoot "."
#define InitialClientBufLen 4096
#define InitialHeadersCapacity 64
#define InitialCookiesCapacity 16
#define InitialURLVarsCapacity 4
#define MaxActiveConnections 256
#define DeathCheckInterval 1000
#define AcceptQueueLen 16

#include <SDL2/SDL.h>
#include <SDL2/SDL_net.h>

struct Worker;
typedef struct Worker Worker;

typedef struct
{
	char* name;
	char* value;
}
NameValuePair;

typedef struct
{
	NameValuePair* pairs;
	size_t capacity,count;
}
NameValueList;

void InitNameValueList (NameValueList* list, size_t initial_capacity);
#define ResetNameValueList(list) ((list).count = 0)
NameValuePair* NewNameValuePair (NameValueList* list);
#define FreeNameValueList(list) (free((list).pairs))

typedef struct
{
	//struct Listener* listener;
	TCPsocket client_socket;
	SDLNet_SocketSet client_socket_set; // only contains client_socket
	char* buf;
	size_t pos,len,capacity;
    jmp_buf on_bad_request;
    //char* error_information;
	//int connid;
	char alive;
	//int handled_requests_count;

	char* method;
	char* path;
	char* version;

	NameValueList cookies,headers,url_vars;

	Worker* worker;
}
Client;



typedef struct
{
	size_t length;
	char* buf;
}
PostData;
PostData GetPostData (Client* client, size_t max_allowed_size);

void SendString (Client* client, char* string);
void HTTP_ResponseHeaders (Client* client, char* error, char* ContentType); /// Pass NULL as ContentType to omit that header.
void HTTP_SendBinaryContent (Client* client, char* content, size_t content_length);
void HTTP_SendStringContent (Client* client, char* content);
void HTTP_SendNothing (Client* client);








int HTTP_Init (); // Zero on success, nonzero on failure.
void HTTP_Quit ();
void StartListening (Uint16 port, void (*CommunicationFunction) (Worker* worker, TCPsocket* client, void* userdata), void* CommunicationFunctionUserdata);

void ServeClient_HTTP (Worker* worker, TCPsocket client_socket, void (*handle_request) (Client* client));

void SMH_StatsStep ();
char StartsWith (char* fullstr, char* startstr);






char EndsWith (char* fullstr, char* endstr);
#define Status200(client,ContentType) HTTP_ResponseHeaders(client,"200 OK",ContentType)
void EmitFile (Client* client, FILE* file, size_t length);
void HTTP_SendFile (Client* client, FILE* file);
void Error404 (Client* client);
char PostDataMatchesString (PostData data, char* str);
#define SendEmpty(client) SendString(client,"Content-Length: 0\r\n\r\n")
void DiscardPostData (Client* client);
void SendHeader (Client* client, char* header_name, char* header_value);
char* GetHeader (Client* client, char* name);







/// C function definitions for C++
#ifdef __cplusplus
}
#endif

#endif // MAIN_H_INCLUDED
