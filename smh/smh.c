#include "smh.h"

#include <ctype.h>

#include <setjmp.h>
#include <time.h>
#include <string.h>



#include "workers.h"
#include "acceptedqueue.h"


void InitNameValueList (NameValueList* list, size_t initial_capacity)
{
	list->pairs = malloc(sizeof(NameValuePair) * initial_capacity);
	list->capacity = initial_capacity;
	list->count = 0;
};

NameValuePair* NewNameValuePair (NameValueList* list)
{
	if (list->count >= list->capacity)
	{
		list->capacity <<= 1;
		list->pairs = realloc(list->pairs,sizeof(NameValuePair) * list->capacity);
	};
	return list->pairs + list->count++;
};

char HexCodeValue (char code)
{
	if (code >= '0' && code <= '9') return code - '0';
	else if (code >= 'a' && code <= 'z') return code - 'a' + 10;
	else return code - 'A' + 10;
	// I don't care about invalid hex characters so it's fine to produce an invalid value.
};
void ReplaceHexChars (char* path)
{
	// resolve %FF notation
	char* readchar = path;
	char* writechar = path;
	char here;
	while (here = *readchar++)
	{
		if (here == '%')
		{
			*writechar = 0;
			if (here = *readchar++) *writechar = HexCodeValue(here) << 4;
			else break;
			if (here = *readchar++) *writechar++ += HexCodeValue(here);
			else break;
		}
		else *writechar++ = here;
	};
	*writechar = 0;
};

void HTTP_ParseFormFields (char* str, NameValueList* store_in)
{
	// this stuff: "n=5&f=2&o=hello"
	while (1)
	{
		NameValuePair pair;
		pair.name = str;
		while (1)
		{
			char here = *str;
			if (here == '&' || here == 0) break;
			else if (here == '=')
			{
				*str++ = 0;
				break;
			};
			str++;
		};
		pair.value = str;
		while (1)
		{
			char here = *str;
			if (here == '&')
			{
				*str++ = 0;
				break;
			}
			else if (!here) break;
			str++;
		};
		//printf("name is [%s]\n",pair.name);
		if (*pair.name)
		{
			ReplaceHexChars(pair.name);
			ReplaceHexChars(pair.value);
			*NewNameValuePair(store_in) = pair;
		};
		if (!*str) break;
	};
};

void HTTP_ParseURLVars (Client* client)
{
	char* path = client->path;
	char herechar;
	while (herechar = *path)
	{
		if (herechar == '?')
		{
			*path++ = 0;
			goto URL_VARS_PRESENT;
		};
		path++;
	};
	return;
	URL_VARS_PRESENT:
	printf("url vars: \"%s\"\n",path);
	HTTP_ParseFormFields(path,&client->url_vars);
	for (int i = 0; i < client->url_vars.count; i++)
	{
		NameValuePair* pair = client->url_vars.pairs + i;
		printf("\t\"%s\"=\"%s\"\n",pair->name,pair->value);
	};
};





typedef struct
{
	Uint16 port;
	void (*CommunicationFunction) (Worker* worker, TCPsocket client_socket, void* userdata);
	void* CommunicationFunctionUserdata;
}
Listener;

volatile char ServerShouldDie = 0;


void TransposeNameValueListStrings (NameValueList* list, size_t transpose)
{
	NameValuePair* pair = list->pairs;
	for (size_t i = 0; i < list->count; i++, pair++)
	{
		pair->name += transpose;
		pair->value += transpose;
	};
};
void ExpandIncomingBuffer (Client* client, size_t newlen)
{
	//printf("client capacity: %llu; required length: %llu\n",client->capacity,newlen);
	if (client->capacity < newlen)
	{
		//printf("expand from %llu ",client->capacity);
		while (client->capacity < newlen) client->capacity <<= 1;
		//printf("to %llu\n",client->capacity);
		char* prev_ptr = client->buf;
		client->buf = realloc(client->buf,client->capacity);
		if (client->buf != prev_ptr)
		{
			//return;

			size_t transpose = client->buf - prev_ptr;
			//printf("transpose by: %llu\n",transpose);
			client->method += transpose;
			client->path += transpose;
			client->version += transpose;
			TransposeNameValueListStrings(&client->headers,transpose);
			TransposeNameValueListStrings(&client->cookies,transpose);
			TransposeNameValueListStrings(&client->url_vars,transpose);
		};
	};
};
void ReceiveMore (Client* client)
{
	/** Don't add to stale on first check,
		because if there are already data to be received,
		then adding this to the queue is a big waste of time,
		and it also makes it possible that a fully active socket gets dropped by the server,
		which would be really stupid. **/

	if (!SDLNet_CheckSockets(client->client_socket_set,0))
	{
		Uint32 stop_waiting_at = SDL_GetTicks() + MaxIdleMilliseconds;
		// avoid performing blocking recv on a tcp socket that could stay around for potentially forever, if there is not already data.

		AddToStale(client->worker);

		int ready;
		while (1)
		{
			ready = SDLNet_CheckSockets(client->client_socket_set,DeathCheckInterval);
			int die = ServerShouldDie || client->worker->die_early || SDL_GetTicks() > stop_waiting_at;
			if (ready || die) break;
		};
		RemoveFromStale(client->worker);
		if (!ready) goto DISCONNECTION;
	};

	ExpandIncomingBuffer(client,client->len + MinRecvLen);

	int received = SDLNet_TCP_Recv(client->client_socket,client->buf + client->len,client->capacity - client->len);
	if (received <= 0) goto DISCONNECTION;
	client->len += received;

	return;

	DISCONNECTION:
	client->alive = 0;
};



void SendString (Client* client, char* string)
{
	SDLNet_TCP_Send(client->client_socket,string,strlen(string));
};
void SendHeader (Client* client, char* header_name, char* header_value)
{
	SendString(client,header_name);
	SendString(client,": ");
	SendString(client,header_value);
	SendString(client,"\r\n");
};
void HTTP_SendContentLength (Client* client, size_t content_length)
{
	char* length_header;
	asprintf(&length_header,"Content-Length: %llu\r\n\r\n",(unsigned long long)content_length);
	SendString(client,length_header);
	free(length_header);
};
void HTTP_SendBinaryContent (Client* client, char* content, size_t content_length)
{
	HTTP_SendContentLength(client,content_length);
	if (content_length) SDLNet_TCP_Send(client->client_socket,content,content_length);
};
void HTTP_SendStringContent (Client* client, char* content)
{
	HTTP_SendBinaryContent(client,content,strlen(content));
};
void HTTP_SendNothing (Client* client)
{
	SendString(client,"Content-Length: 0\r\n\r\n");
};
void HTTP_ResponseHeaders (Client* client, char* error, char* ContentType)
{
	SendString(client,"HTTP/1.1 ");
	SendString(client,error);
	SendString(client,"\r\n");
	SendHeader(client,"Server","SMHproto by Seth");
	if (ContentType) SendHeader(client,"Content-Type",ContentType);
};
void BadRequest (Client* client, char* info)
{
	//client->error_information = info;
	longjmp(client->on_bad_request,1);
};
void HTTP_EndAtChar (Client* client, char end_at, size_t max_length)
{
	while (1)
	{
		if (client->pos >= client->len)
		{
			if (client->alive) ReceiveMore(client);
			else BadRequest(client,"Expected character.");
		};
		char* hereptr = client->buf + client->pos++;
		if (*hereptr == end_at)
		{
			*hereptr = 0;
			break;
		};
		if (!max_length--) BadRequest(client,"too long[search character]");
	};
};
char HTTP_IsEndReached (Client* client)
{
	while (client->pos >= client->len)
	{
		if (client->alive) ReceiveMore(client);
		else return 1;
	};
	return 0;
};
void HTTP_EndAtCRLF (Client* client, size_t max_length)
{
	char* hereptr;
	while (1)
	{
		if (client->pos >= client->len)
		{
			if (client->alive) ReceiveMore(client);
			else BadRequest(client,"client disconnect");
		};
		hereptr = client->buf + client->pos++;
		if (*hereptr == '\r')
		{
			if (!HTTP_IsEndReached(client))
				if (client->buf[client->pos] == '\n') client->pos++;
			break;
		}
		else if (*hereptr == '\n') break;
		if (!max_length--) BadRequest(client,"too long [crlf find]");
	};
	*hereptr = 0;
};
void FailIfAtEnd (Client* client)
{
	if (HTTP_IsEndReached(client)) BadRequest(client,"ended early");
};
char HTTP_IsHereCRLF (Client* client)
{
	FailIfAtEnd(client);
	char cc = client->buf[client->pos];
	if (cc == '\r')
	{
		client->pos++;
		if (!HTTP_IsEndReached(client))
			if (client->buf[client->pos] == '\n') client->pos++;
	}
	else if (cc == '\n') client->pos++;
	else return 0;
	return 1;
};
void HTTP_EndAtSpace (Client* client, size_t max_len)
{
	HTTP_EndAtChar(client,' ',max_len);
};
void HTTP_ReadMethod (Client* client)
{
	client->method = client->buf + client->pos;
	HTTP_EndAtSpace(client,MaxMethodLen);
};
void AllSlashesForward (char* path)
{
	char here;
	while (here = *path)
	{
		if (here == '\\') *path = '/';
		path++;
	};
};
void RemoveDotDirectories (char* path)
{
	//printf("prior to remove dots \"%s\"\n",path);
	char* readptr = path;
	char* writeptr = path;
	char dots = 0;
	char onlydots = 1;
	while (1)
	{
		char here = *readptr++;
		if (here == '/' || !here)
		{
			if (/*dots == 2 &&*/ onlydots)
			{
				writeptr -= dots + 1;
				if (writeptr < path) writeptr = path;
			};
			dots = 0;
			onlydots = 1;
		}
		else if (here == '.' || isspace(here)) dots++;
		else onlydots = 0;

		*writeptr++ = here;

		if (!here) break;
	};
	//printf("after remove dots \"%s\"\n",path);
};
/*void AdvancePastSlashes (char** path)
{
	while (**path == '/') (*path)++;
};*/
/*void AdvancePastSpaces (char** path)
{
	while (isspace(**path)) (*path)++;
};*/
void SanitizePath (Client* client)
{
	/// Expectation is that "%XX" characters have already been converted by this point.
	//AdvancePastSpaces(&client->path);
	AllSlashesForward(client->path);
	RemoveDotDirectories(client->path);
	//AdvancePastSlashes(&client->path);
	/// Due to the behavior of RemoveDotDirectories, there will be a maximum of one slash at the beginning of the string, before non-slash characters.
	if (*client->path == '/') client->path++;
};
void HTTP_ReadPath (Client* client)
{
	client->path = client->buf + client->pos;

	HTTP_EndAtSpace(client,MaxPathLen);

	//printf("raw path: \"%s\"\n",client->path);

	HTTP_ParseURLVars(client);

	// Convert the hex codes *after* reading the path variables.
	// Otherwise '%XX' for '?', '&', '=' could screw things up.

	ReplaceHexChars(client->path);

	SanitizePath(client);

	//printf("fully sanitized path: \"%s\"\n",client->path);
};
void HTTP_ReadVersion (Client* client)
{
	client->version = client->buf + client->pos;
	HTTP_EndAtCRLF(client,MaxVersionLen);
};
void HTTP_EndHeaderName (Client* client)
{
	HTTP_EndAtChar(client,':',MaxHeaderNameLen);
	FailIfAtEnd(client);
	if (client->buf[client->pos] == ' ') client->pos++;
};
void ParseCookies (Client* client, char* cookies_str)
{
	while (1)
	{
		//printf("scanning for spaces\n");
		char here;
		while (here = *cookies_str)
		{
			if (here == ' ') cookies_str++;
			else break;
		};

		//printf("about to read from: \"%s\"\n",cookies_str);

		NameValuePair thiscookie;
		thiscookie.name = cookies_str;
		while (here = *cookies_str)
		{
			if (here == '=')
			{
				*cookies_str++ = 0;
				goto GOT_NAME;
			};
			cookies_str++;
		};
		break;
		GOT_NAME:
		//printf("got name: %s\n",thiscookie.name);
		thiscookie.value = cookies_str;
		while (here = *cookies_str)
		{
			if (here == ';')
			{
				*cookies_str++ = 0;
				break;
			};
			cookies_str++;
		};
		//printf("got value: %s\n",thiscookie.value);
		//NewCookie(client,thiscookie);
		*NewNameValuePair(&client->cookies) = thiscookie;
	};
	/*for (int i = 0; i < client->cookies_count; i++)
	{
		printf("cookie \"%s\" is \"%s\"\n",client->cookies[i].name,client->cookies[i].value);
	};*/
};
void HTTP_ReadHeader (Client* client)
{
	NameValuePair header;
	header.name = client->buf + client->pos;
	HTTP_EndHeaderName(client);
	header.value = client->buf + client->pos;
	HTTP_EndAtCRLF(client,MaxHeaderValueLen);

	if (!strcmp(header.name,"Cookie")) ParseCookies(client,header.value);
	else *NewNameValuePair(&client->headers) = header;
};
void HTTP_ReadRequest (Client* client)
{
	//client->cookies_string = NULL;
	HTTP_ReadMethod(client);
	HTTP_ReadPath(client);
	HTTP_ReadVersion(client);
	while (1)
	{
		if (HTTP_IsHereCRLF(client)) break;
		HTTP_ReadHeader(client);
	};

	//printf("Headers:\n");
	/*printf("Cookies:\n");
	for (size_t i = 0; i < client->headers_count; i++)
	{
		HTTP_Header* header = client->headers + i;
		if (!strcmp(header->name,"Cookie"))
			printf("\t%s: %s\n",header->name,header->value);
	};*/


};
void FlushBuffer (Client* client)
{
	client->len -= client->pos;
	if (client->len) memmove(client->buf,client->buf + client->pos,client->len);
	client->pos = 0;
};


char* BadRequestPage =
	"<!DOCTYPE html>\n"
	"<html>\n"
	"	<head>\n"
	"		<title>HTTP 400: Bad Request</title>\n"
	"	</head>\n"
	"	<body>\n"
	"		<h1>HTTP Client Error 400: Bad Request</h1>\n"
	"		"
		"<p>"
		"The server was unable to correctly process the client's HTTP request syntax."
		" "
		"This may have been due to a bug in the server's HTTP request parser, or the client's HTTP request generation."
		"</p>"
		"\n"
//	"		<p>Specific error description: \"%s\"</p>\n"
	"	</body>\n"
	"</html>\n"
;

char* PageContent_Error404 =
	"<!DOCTYPE html>\n"
	"<html>\n"
	"	<head>\n"
	"		<title>HTTP 404: Not Found</title>\n"
	"	</head>\n"
	"	<body>\n"
	"		<h1>HTTP Client Error 404: Resource Not Found</h1>\n"
	"		"
		"<p>"
		"The server was unable to locate the requested resource."
		" "
		"This may have been caused by a mistyped or broken link."
		"</p>"
		"\n"
	"	</body>\n"
	"</html>\n"
;
void EmitFile (Client* client, FILE* file, size_t length)
{
	char outbuf [FileReadBufferSize];
	while (length)
	{
		size_t outsize = length;
		if (outsize > FileReadBufferSize) outsize = FileReadBufferSize;
		length -= outsize;

		fread(outbuf,1,outsize,file);
		SDLNet_TCP_Send(client->client_socket,outbuf,outsize);
	};
};
void HTTP_SendFile (Client* client, FILE* file)
{
	fseek(file,0,SEEK_END);
	size_t length = ftell(file);
	fseek(file,0,SEEK_SET);

	HTTP_SendContentLength(client,length);

	EmitFile(client,file,length);

	fclose(file);
};
void ExpectByteCount (Client* client, size_t expected)
{
	size_t until = client->pos + expected;
	while (client->len < until)
	{
		if (client->alive) ReceiveMore(client);
		else BadRequest(client,"expected more bytes than received");
	};
};
size_t InterpretUnsignedDecimal (char* str, char* wasvalid /* points to one status character in caller's stack */)
{
	size_t out = 0;
	char cc = *str++;
	if (!cc) goto INVALID;
	do
	{
		if (cc < '0' || cc > '9') goto INVALID;
		out *= 10;
		out += cc - '0';
	}
	while (cc = *str++);
	*wasvalid = 1;
	return out;
	INVALID:
	*wasvalid = 0;
};
char* GetHeader (Client* client, char* name)
{
	NameValuePair* header = client->headers.pairs;
	for (size_t i = 0; i < client->headers.count; i++, header++)
		if (!strcmp(header->name,name))
			return header->value;
	return NULL;
};
void DiscardData (Client* client, size_t content_length)
{
	while (content_length)
	{
		FlushBuffer(client);
		if (!client->len)
		{
			if (client->alive) ReceiveMore(client);
			else BadRequest(client,"expected more during post data discard");
		};
		size_t receive = client->len;
		if (receive > content_length) receive = content_length;
		content_length -= receive;
		client->pos += receive;
	};
};
void DiscardPostData (Client* client)
{
	char* ContentLength = GetHeader(client,"Content-Length");
	if (!ContentLength) BadRequest(client,"no content length header");
	char wasvalid;
	size_t content_length = InterpretUnsignedDecimal(ContentLength,&wasvalid);
	if (!wasvalid) BadRequest(client,"invalid content length value");
	DiscardData(client,content_length);
};
PostData GetPostData (Client* client, size_t max_allowed_size)
{
	char* ContentLength = GetHeader(client,"Content-Length");
	if (!ContentLength) BadRequest(client,"no content length header");
	char wasvalid;
	size_t content_length = InterpretUnsignedDecimal(ContentLength,&wasvalid);
	if (!wasvalid) BadRequest(client,"invalid content length value");
	if (content_length > max_allowed_size)
	{
		DiscardData(client,content_length);
		BadRequest(client,"post data too long");
	};
	ExpectByteCount(client,content_length);
	PostData out;
	out.buf = client->buf + client->pos;
	out.length = content_length;
	client->pos += out.length;
	return out;
};
void PostDataToFile (Client* client, FILE* file)
{
	char* ContentLength = GetHeader(client,"Content-Length");
	char wasvalid;
	size_t content_length = InterpretUnsignedDecimal(ContentLength,&wasvalid);
	if (!wasvalid) goto INVALID;

	while (content_length)
	{
		FlushBuffer(client);
		if (!client->len)
		{
			if (client->alive) ReceiveMore(client);
			else goto INVALID;
		};
		size_t receive = client->len;
		if (receive > content_length) receive = content_length;
		content_length -= receive;
		fwrite(client->buf,1,receive,file);
		client->pos += receive;
	};
	fclose(file);
	return;

	INVALID:
	fclose(file);
	BadRequest(client,"failed writing post data to file");
};
char PostDataMatchesString (PostData data, char* str)
{
	for (size_t i = 0; i < data.length; i++)
	{
		char strchar = *str++;
		if (!strchar || strchar != data.buf[i]) return 0;
	};
	return !*str;
};
void Error404 (Client* client)
{
	HTTP_ResponseHeaders(client,"404 Not Found","text/html");
	HTTP_SendStringContent(client,PageContent_Error404);
};

char EndsWith (char* fullstr, char* endstr)
{
	size_t full_len = strlen(fullstr);
	size_t end_len = strlen(endstr);
	if (full_len < end_len) return 0;
	fullstr += (full_len - end_len);
	while (end_len--)
		if (*fullstr++ != *endstr++) return 0;
	return 1;
};

SDL_atomic_t RequestsServed = {0}; // gets reset on an interval
SDL_atomic_t OpenConnections = {0}; // does not include accept queue
time_t InitializedAtTime; // used for measuring uptime
Uint64 RequestsEverServed = 0; // never gets reset
Uint64 RequestsServedLastSecond = 0; // resets every second




void ServeClient_HTTP (Worker* worker, TCPsocket client_socket, void (*handle_request) (Client* client))
{
	SDL_AtomicIncRef(&OpenConnections);



	Client client_storage;
	Client* client = &client_storage;
	client->client_socket = client_socket;
	client->worker = worker;



	client->client_socket_set = SDLNet_AllocSocketSet(1);
	SDLNet_AddSocket(client->client_socket_set,client->client_socket);


	client->buf = malloc(InitialClientBufLen);
	client->capacity = InitialClientBufLen;
	client->len = 0;
	client->pos = 0;
	client->alive = 1;

	InitNameValueList(&client->headers,InitialHeadersCapacity);
	InitNameValueList(&client->cookies,InitialCookiesCapacity);
	InitNameValueList(&client->url_vars,InitialURLVarsCapacity);

	//client->error_information = "uninitialized error string";

	if (setjmp(client->on_bad_request))
	{
		if (client->alive)
		{
//			SDL_LockMutex(PrintingMutex);
			//printf("Error!\n");
			//SDL_UnlockMutex(PrintingMutex);
			HTTP_ResponseHeaders(client,"400 Bad Request","text/html");
			//char* errorpage;
			//asprintf(&errorpage,BadRequestPage,client->error_information);
			//HTTP_SendStringContent(client,errorpage);
			//free(errorpage);
			HTTP_SendStringContent(client,BadRequestPage);
		}
		//else printf("disconnection\n");
		goto CLEANUP;
	};





	while (1)
	{
		FlushBuffer(client);
		ResetNameValueList(client->headers);
		ResetNameValueList(client->cookies);
		ResetNameValueList(client->url_vars);

		HTTP_ReadRequest(client);
		handle_request(client);
		SDL_AtomicIncRef(&RequestsServed);

		if (ServerShouldDie || worker->die_early)
		{
			/// The server needs to shut down, stop accepting requests.
			/// Or, new connections have come in that need to be answered immediately, so don't wait on another message from this one.
			break;
		};
	};

	CLEANUP:

	SDL_AtomicDecRef(&OpenConnections); // this is just a statistic

	SDLNet_FreeSocketSet(client->client_socket_set);
	SDLNet_TCP_Close(client->client_socket);

	//printf("about to free %llX, %llX, and %llX\n",client->headers.pairs,client->cookies.pairs,client->url_vars.pairs);
	FreeNameValueList(client->headers);
	FreeNameValueList(client->cookies);
	FreeNameValueList(client->url_vars);
};










SDL_atomic_t ListenersActive = {0};
void Listen (Listener* listener_object)
{
	IPaddress listen_addr;
	SDLNet_ResolveHost(&listen_addr,NULL,listener_object->port);
	TCPsocket listener = SDLNet_TCP_Open(&listen_addr);
	if (!listener)
	{
		printf("Failed to listen on port %d, maybe it is occupied by another service.\n",listener_object->port);
		goto CANT_LISTEN;
	};
	SDLNet_SocketSet listeners = SDLNet_AllocSocketSet(1);
	SDLNet_AddSocket(listeners,listener);

	printf("Listening on port %d\n",listener_object->port);

	while (1)
	{
		SDLNet_CheckSockets(listeners,DeathCheckInterval);
		TCPsocket client;

		// check for death signal *directly prior to* when semwait would be called, because
		// semwait inside this accept loop will block forever if the server has already died and ensured no connections are still around.
		//if (listener_object->die) break;
		if (ServerShouldDie) break;

		while (client = SDLNet_TCP_Accept(listener))
		{
			SocketAndFunction sock_and_func;
			sock_and_func.connection = client;
			sock_and_func.CommunicationFunction = listener_object->CommunicationFunction;
			sock_and_func.CommunicationFunctionUserdata = listener_object->CommunicationFunctionUserdata;
			OnAcceptSocket(sock_and_func);
		};
	};

	SDLNet_FreeSocketSet(listeners);
	SDLNet_TCP_Close(listener);

	CANT_LISTEN: // jump here if the server socket couldn't be created - therefore dont destroy it, or its socket set since they dont exist

	SDL_AtomicDecRef(&ListenersActive); // do not signal this until sdl/sdlnet close functions have finished calling
	free(listener_object);
};
void StartListening (Uint16 port, void (*CommunicationFunction) (Worker* worker, TCPsocket* client, void* userdata), void* CommunicationFunctionUserdata)
{
	Listener* listener = malloc(sizeof(Listener));
	listener->port = port;
	listener->CommunicationFunction = CommunicationFunction;
	listener->CommunicationFunctionUserdata = CommunicationFunctionUserdata;
	SDL_DetachThread(SDL_CreateThread(Listen,"TCP Listener Monitor",listener));
	SDL_AtomicIncRef(&ListenersActive);

	//return listener;
};












int HTTP_Init ()
{
	if (SDL_Init(0))
	{
		printf("Couldn't initialize core SDL.\n");
		return -1;
	};

	if (SDLNet_Init())
	{
		printf("Couldn't initialize SDL_net.\n");
		return -2;
	};

	InitAllWorkers();
	InitAccepter();
	InitializedAtTime = time(NULL);
	return 0;
};
void HTTP_Quit ()
{
	ServerShouldDie = 1;
	while (SDL_AtomicGet(&ListenersActive)) SDL_Delay(100);

	QuitAccepter();
	WaitAllWorkersExit();

	SDLNet_Quit();
	SDL_Quit();
};

void SMH_StatsStep ()
{
	RequestsServedLastSecond = SDL_AtomicSet(&RequestsServed,0);
	RequestsEverServed += RequestsServedLastSecond;
	//printf("Requests Served: %d | Connections Open: %d\n",SDL_AtomicSet(&RequestsServed,0),SDL_AtomicGet(&OpenConnections));
};

char StartsWith (char* fullstr, char* startstr)
{
	while (1)
	{
		char fullchar = *fullstr++;
		char startchar = *startstr++;
		if (!startchar) return 1;
		if (fullchar != startchar) return 0;
	};
};
