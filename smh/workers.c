#include <SDL2/SDL.h>
#include "workers.h"





/** Idle workers are ones that are not communicating with a live socket. **/
SDL_mutex* IdleQueueLock; // Synchronizes access to queue globals.
Worker* FirstIdleWorker = NULL; // Used for getting the first available.
Worker* LastIdleWorker = NULL; // Used for appending to the queue.

void AddToIdle (Worker* worker)
{
	worker->next_idle = NULL;

	SDL_LockMutex(IdleQueueLock);

	if (!FirstIdleWorker) FirstIdleWorker = worker;
	if (LastIdleWorker) LastIdleWorker->next_idle = worker;
	LastIdleWorker = worker;

	SDL_UnlockMutex(IdleQueueLock);
};
Worker* GetFirstIdle ()
{
	SDL_LockMutex(IdleQueueLock);

	Worker* out = FirstIdleWorker;
	if (out)
	{
		FirstIdleWorker = out->next_idle;
		if (LastIdleWorker == out) LastIdleWorker = NULL;
	};

	SDL_UnlockMutex(IdleQueueLock);

	return out;
};



/** Stale workers are ones that have not disconnected but are waiting to receive.
	If there are no workers in the Idle Queue, then a stale worker can be told to disconnect early,
	instead of waiting around for the entire duration of the Keep Alive Timeout. **/
SDL_mutex* StaleQueueLock; // Don't fuck around with the list without locking it down!
Worker* FirstStaleWorker = NULL; // Used for checking the most-stale.
Worker* LastStaleWorker = NULL; // Used for appending next stale to the end.

void AddToStale (Worker* worker)
{
	worker->next_stale = NULL;

	SDL_LockMutex(StaleQueueLock);

	if (!FirstStaleWorker) FirstStaleWorker = worker;
	worker->prev_stale = LastStaleWorker;
	if (LastStaleWorker) LastStaleWorker->next_stale = worker;
	LastStaleWorker = worker;

	SDL_UnlockMutex(StaleQueueLock);
};
void RemoveFromStale_Internal (Worker* worker)
{
	if (FirstStaleWorker == worker) FirstStaleWorker = worker->next_stale;
	if (LastStaleWorker == worker) LastStaleWorker = worker->prev_stale;
	if (worker->prev_stale) worker->prev_stale->next_stale = worker->next_stale;
	if (worker->next_stale) worker->next_stale->prev_stale = worker->prev_stale;
};
void RemoveFromStale (Worker* worker)
{
	SDL_LockMutex(StaleQueueLock);

	RemoveFromStale_Internal(worker);

	SDL_UnlockMutex(StaleQueueLock);
};
Worker* GetFirstStale ()
{
	SDL_LockMutex(StaleQueueLock);

	Worker* out = FirstStaleWorker;
	if (out) RemoveFromStale_Internal(out);

	SDL_UnlockMutex(StaleQueueLock);

	return out;
};



Worker Workers [MaxActiveConnections];
Worker* AcquireWorker ()
{
	Worker* worker = GetFirstIdle();
	if (worker) goto GOT_WORKER;
	else
	{
		worker = GetFirstStale();
		if (!worker) worker = Workers; // No stale ones so just kill Workers[0].

		worker->die_early = 1;
		SDL_SemWait(worker->finish); // Wait till it signals death.
		SDL_SemPost(worker->finish); // Allow next SemWait to instantly succeed.

		worker = GetFirstIdle();
		// Now guaranteed to exist, though it may not give the current value of `worker`,
		// if another worked became idle while this one was still in process of being killed.


	};
	GOT_WORKER:
	SDL_SemWait(worker->finish);
	return worker;
};
void StartWorker (SocketAndFunction connection)
{
	Worker* worker = AcquireWorker();
	worker->die_early = 0;
	worker->socket_and_function = connection;
	SDL_SemPost(worker->start);
};
void WorkerProc (Worker* worker)
{
	while (1)
	{
		/*if (!worker->die_early)*/ AddToIdle(worker);
		worker->die_early = 0;
		SDL_SemPost(worker->finish);

		SDL_SemWait(worker->start);

		//printf("[worker] connection: %llX\n",worker->connection);
		TCPsocket connection = worker->socket_and_function.connection;
		if (connection) worker->socket_and_function.CommunicationFunction(worker,connection,worker->socket_and_function.CommunicationFunctionUserdata);
		else break;
	};
};
void WaitAllWorkersExit ()
{
	for (size_t i = 0; i < MaxActiveConnections; i++)
	{
		Worker* worker = Workers + i;
		worker->die_early = 1;
		SDL_SemWait(worker->finish);
	};
	for (size_t i = 0; i < MaxActiveConnections; i++)
	{
		Worker* worker = Workers + i;
		worker->socket_and_function.connection = NULL;
		SDL_SemPost(worker->start);
	};
	for (size_t i = 0; i < MaxActiveConnections; i++)
	{
		Worker* worker = Workers + i;
		SDL_WaitThread(worker->thread,NULL);
		SDL_DestroySemaphore(worker->start);
		SDL_DestroySemaphore(worker->finish);
	};
	SDL_DestroyMutex(IdleQueueLock);
	SDL_DestroyMutex(StaleQueueLock);
};
void InitAllWorkers ()
{
	IdleQueueLock = SDL_CreateMutex();
	StaleQueueLock = SDL_CreateMutex();
	for (size_t i = 0; i < MaxActiveConnections; i++)
	{
		Worker* worker = Workers + i;
		worker->start = SDL_CreateSemaphore(0);
		worker->finish = SDL_CreateSemaphore(0);
		worker->thread = SDL_CreateThread(WorkerProc,"Client Communication Thread",worker);
	};
};





